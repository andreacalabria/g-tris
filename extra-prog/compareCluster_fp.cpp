#include <stdio.h>
#include <string>
#include <fstream>
#include <iostream>
#include <ostream>
#include <sstream>
#include <map>

#define DIST 4

using namespace std;

void help() {
    cout << "*************************\n";
    cout << "Program CompareCluster:";
    cout << endl;
    cout << "find clusters in \"cluster_1.bed\" file that are ";
    cout << "missing in \"cluster_2.bed\" file.";
    cout << endl << endl;
    cout << "comparecluster <cluster_1.bed> <cluster_2.bed> < all | sin | rep >";
    cout << endl;
    cout << "*************************\n";
}


//Int to String conversion
string convertInt(int number) {
    stringstream ss;
    ss << number;
    return ss.str();
}

ostream& operator<< (ostream& os,
                          const pair< string, int>& p) {
    os << p.first << "-" << p.second;
    return os;
}

int main(int argc, char* argv[]) {
    //Check number of arguments
    if(argc != 4) {
        help();
        return 1;
    }
    // Use mode:
    // sin -> only single labels
    // rep -> only repeat labels
    // all -> both single and repeat labels
    string mode(argv[3]);
    if(mode != "all" && mode != "sin" && mode != "rep") {
	cerr << "Usage mode error.\n";
	help();
	return 1;
    }
    //Open input streams
    ifstream clu1(argv[1]);
    ifstream clu2(argv[2]);

    //Check if streams are open
    if(!clu1.is_open() || !clu2.is_open()) {
        cerr << "Error in opening input file(s):";
        cerr << endl;
        cerr << argv[1] << "\n" << argv[2];
        cerr << endl;
        return 1;
    }
    string str_out = mode + ",";
    cout << "*************************\n";
    cout << "CLUSTER 1\n";
    cout << "*************************\n";
    //Memorize clu1 clusters in a map
    //Note: it is assumed that clu1 clusters have single labels
    map< pair< string, int >, string > c1;
    string line;
    int n_clu1 = 0;
    getline(clu1, line);
    while(getline(clu1, line)) {
        istringstream ss(line);
        string chr = "", cname = "";
        int ipos = 0, fpos = 0;
        ss >> chr >> ipos >> fpos >> cname;
        map< pair< string, int >,
		  string >::iterator it;
        if((it = c1.find(make_pair(chr, ipos))) == c1.end()) {
            ++n_clu1;
            if(cname == "") {
                cname = "Cluster" + convertInt(n_clu1);
            }
            c1.insert(make_pair(make_pair(chr, ipos), cname));
        } else {
            cname = it->second;
            cerr << "Duplicated cluster1: " << cname << endl;
	    cerr << "chr: " << chr << " -- pos: " << ipos << "\n";
	    return 1;
	}
    }
    str_out += convertInt(c1.size()) + ",";
    cout << "Num. Cluster1 clusters: " << c1.size() << endl;

    //Memorize clu2 clusters in a map
    cout << "*************************\n";
    cout << "CLUSTER 2\n";
    cout << "*************************\n";
    multimap< pair< string, int >, string > c2;
    multimap< string, pair< string, int > > inv_c2;
    map< string, bool > c2_used;
    int n_mis_clu = 0;
    getline(clu2, line);
    while(getline(clu2, line)) {
        istringstream ss(line);
        string chr = "", cname = "";
        int ipos = 0, fpos = 0;
        ss >> chr >> ipos >> fpos >> cname;
	if(cname == "") {
	    ++n_mis_clu;
	    cname = "Cluster" + convertInt(n_mis_clu);
	}
	c2.insert(make_pair(make_pair(chr, ipos), cname));
	inv_c2.insert(make_pair(cname, make_pair(chr, ipos)));
	c2_used.insert(make_pair(cname, false));
    }

    // Count duplicated labels
    int num_dup_lab = 0;
    multimap< pair< string, int>, string >::iterator c2_it;
    for(c2_it = c2.begin(); c2_it != c2.end(); c2_it = c2.upper_bound(c2_it->first)) {
	if(c2.count(c2_it->first) > 1) {
	    num_dup_lab++;
	}
    }

    // Count single and repeated clusters
    int num_clu2 = 0;
    int num_clu2_sing = 0;
    multimap< string,
                   pair< string, int > >::iterator mm_it;
    for(mm_it = inv_c2.begin(); mm_it != inv_c2.end(); mm_it = inv_c2.upper_bound(mm_it->first)) {
	num_clu2++;
	if(inv_c2.count(mm_it->first) == 1) {
	    num_clu2_sing++;
	}
    }
    str_out += convertInt(num_clu2) + ",";
    cout << "Num. Cluster2 clusters: " << num_clu2 << endl;
    cout << "Single Cluster2 clusters: " << num_clu2_sing << endl;
    cout << "Duplicated Cluster2 labels: " << num_dup_lab << endl;

    // COMPARISON
    cout << "*************************\n";
    cout << "COMPARISON\n";
    cout << "*************************\n";
    string bed1_file(argv[1]);
    size_t found = bed1_file.find_last_of(".");
    string out_found = bed1_file.substr(0,found) + ".found." + bed1_file.substr(found+1);
    ofstream out_found_stream(out_found.c_str());
    if(!out_found_stream.is_open()) {
	cerr << "Error in creating " << out_found << " file." << endl;
	return 1;
    }
    out_found_stream << "track name=found_clu "
		     << "description=\"clusters found\"\n";

    int num_clu1_found = 0;
    int num_clu2_used_single = 0;
    //Look for single cluster (i.e. the ones having a single label)
    if(mode == "all" || mode == "sin") {
	for(mm_it = inv_c2.begin(); mm_it != inv_c2.end(); mm_it = inv_c2.upper_bound(mm_it->first)) {    
	    if(inv_c2.count(mm_it->first) == 1) {
		multimap< pair< string, int >,
			       string >::iterator it_found;
		string chr = mm_it->second.first;
		for(int i = -DIST; i <= DIST; ++i) {
		    int pos = mm_it->second.second + i;
		    it_found = c1.find(make_pair(chr, pos));
		    if(it_found != c1.end()) {
			num_clu1_found++;
			num_clu2_used_single++;
			out_found_stream << it_found->first.first << "\t"
					 << it_found->first.second << "\t"
					 << it_found->first.second + 1 << "\t"
					 << it_found->second << "\n";
			c2_used[mm_it->first] = true;
			c1.erase(it_found);
			continue;
		    }
		}
	    }
	}
	cout << "Looking for one-to-one label clusters" << endl;
	cout << "Num. Found Cluster1 clusters: " << num_clu1_found << endl;
	cout << "Num. Remaining Cluster1 clusters: " << c1.size() << endl;
    }

    //Look for the remaining ones
    if(mode == "all" || mode == "rep") {
	for(mm_it = inv_c2.begin(); mm_it != inv_c2.end(); mm_it = inv_c2.upper_bound(mm_it->first)) {
	    bool found = false;
	    if(inv_c2.count(mm_it->first) > 1) {
		pair<  multimap< string, pair< string, int > >::iterator,
		       multimap< string, pair< string, int > >::iterator > c_it;
		c_it = inv_c2.equal_range(mm_it->first);
		multimap< string, pair< string, int > >::iterator l_it;
		for(l_it = c_it.first; l_it != c_it.second; ++l_it) {
		    multimap< pair< string, int >,
			      string >::iterator it_found;
		    string chr = l_it->second.first;
		    for(int i = -DIST; i <= DIST; ++i) {
			int pos = l_it->second.second + i;
			it_found = c1.find(make_pair(chr, pos));
			if(it_found != c1.end()) {
			    num_clu1_found++;
			    out_found_stream << it_found->first.first << "\t"
					     << it_found->first.second << "\t"
					     << it_found->first.second + 1 << "\t"
					     << it_found->second << "\n";
			    c2_used[mm_it->first] = true;
			    c1.erase(it_found);
			    found = true;
			    continue;
			}
		    }
		    if(found) continue;
		}
	    }
	}
	cout << "Looking for multi-to-one label clusters" << endl;	
	cout << "Num. Found Cluster1 clusters: " << num_clu1_found << endl;
	cout << "Num. Remaining Cluster1 clusters: " << c1.size() << endl;
    }
    str_out += convertInt(num_clu1_found) + ",";
    str_out += convertInt(num_clu2_sing) + ",";
    str_out += convertInt(num_clu2_used_single) + "\n";

    out_found_stream.flush();
    out_found_stream.close();
    
    //Print missing clusters
    string out_missing = bed1_file.substr(0,found) + "." + mode + "_mis." + bed1_file.substr(found+1);
    ofstream out_missing_stream(out_missing.c_str());
    if(!out_missing_stream.is_open()) {
	cerr << "Error in creating " << out_missing << " file." << endl;
	return 1;
    }
    out_missing_stream << "track name=miss_clus "
		       << "description=\"missing clusters after single comparison\"\n";
    map< pair< string, int>, string >::iterator mis_it;
    cout << "*************************\n";
    cout << "Missing Cluster1 clusters:" << endl;
    for(mis_it = c1.begin(); mis_it != c1.end(); ++mis_it) {
	out_missing_stream << mis_it->first.first << "\t"
			   << mis_it->first.second << "\t"
			   << mis_it->first.second  + 1 << "\t"
			   << mis_it->second << "\n";
	cout << mis_it->first << "-" << mis_it->second << endl;
    }
    out_missing_stream.flush();
    out_missing_stream.close();
    cout << "*************************\n";
    
    //Print false positive clusters
    string out_falpos = bed1_file.substr(0,found) + "." + mode + "_falpos." + bed1_file.substr(found+1);
    ofstream out_falpos_stream(out_falpos.c_str());
    if(!out_falpos_stream.is_open()) {
	cerr << "Error in creating " << out_falpos << " file." << endl;
	return 1;
    }
    out_falpos_stream << "track name=falpos_clus "
		       << "description=\"false positive clusters after comparison\"\n";
    map< string, bool >::iterator fp_it;
    cout << "*************************\n";
    cout << "False Positive clusters:" << endl;
    for(fp_it = c2_used.begin(); fp_it != c2_used.end(); ++fp_it) {
	if(!fp_it->second) {
	  out_falpos_stream << fp_it->first << "\t"
//			      << mis_it->first.second << "\t"
//			      << mis_it->first.second  + 1 << "\t"
//			    << mis_it->second 
	  << "\n";
	  //cout << fp_it->first << endl;
	}
    }
    out_falpos_stream.flush();
    out_falpos_stream.close();
    cout << "*************************\n";
    cout << str_out;
    cout << endl;
    return 0;
}
