/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#include "log.hpp"
#include "alignments.hpp"
#include "configuration.hpp"
#include "subgraph_SNAP.hpp"
#include "iscluster.hpp"
#include "uniqueseqs.hpp"
#include "consensus.hpp"

#include <queue>
#include <iterator>

#include <serial/serial.hpp>
#include <serial/objostr.hpp>
#include <serial/objistr.hpp>

//Kseq fasta parser
#include <zlib.h>
#include <stdio.h>
#include "kseq.h"

USING_NCBI_SCOPE;
USING_SCOPE(objects);

class CreateClusters : public CNcbiApplication {
    virtual void Init(void);
    virtual int Run(void);
    virtual void Exit();
    void readGraph_SNAP(::SubGraph_SNAP& g, ifstream& in_subg_stream);
    std::list< ISCluster > initialClusters_SNAP(::SubGraph_SNAP& g,
						const std::map< string, Alignments >& seq_aln,
						const std::map< string, string >& cons_map,
						ofstream& out_bed_stream,
						ofstream& out_dot_stream,
						const bool approx);
};

void CreateClusters::Init(void) {
auto_ptr<CArgDescriptions> arg_desc(new CArgDescriptions);
    // Specify USAGE context
    CNcbiApplication::DisableArgDescriptions();

    arg_desc->SetUsageContext(GetArguments().GetProgramBasename(),
                              "Cluster Creation program");

    arg_desc->AddKey
        ("i", "consAsnFile",
         "Input file in ASN format containing "                         \
         "the alignments of iterativealignment program.",               \
         CArgDescriptions::eInputFile, CArgDescriptions::fPreOpen);

    arg_desc->AddKey
        ("f", "consFastaFile",
         "Input file in Fasta format containing "                       \
         "the consensus sequences.",                                    \
         CArgDescriptions::eInputFile, CArgDescriptions::fPreOpen);

    arg_desc->AddKey
        ("subg", "subgFile",
         "Input SubGraph file.",
         CArgDescriptions::eInputFile);

    arg_desc->AddOptionalKey
        ("m", "mapFile",
         "File containing the map of unique sequences " \
         "obtained with option '-uniqueseqs' of "       \
         "makeblastdb program.",
         CArgDescriptions::eInputFile);

    arg_desc->AddDefaultKey
        ("num_threads", "num_threads",
         "Number of threads used by BLAST. Default: 1.",
         CArgDescriptions::eInteger, BLAST_THREADS);

    arg_desc->AddFlag("no-consensus",
                      "Disable final consensus computation. "	\
		      "No computation is set.",			\
		      true);
    
    arg_desc->AddFlag("approx",
                      "Compute potentially not the optimal "	\
                      "cluster at each iteration. "		\
		      "TRUE if set, FALSE if not set.",
		      true);

    // Setup arg.descriptions for this application
    SetupArgDescriptions(arg_desc.release());
}

void CreateClusters::readGraph_SNAP(::SubGraph_SNAP& g, ifstream& in_subg_stream) {
    readSerializedGraph(g, in_subg_stream);
    if (g.num_subgraphs == 0 || g.filtered) {
        return;
    } else {
        g.subgraphs = new ::SubGraph_SNAP[g.num_subgraphs];
        for (int i = 0; i < g.num_subgraphs; ++i) {
            readGraph_SNAP(g.subgraphs[i], in_subg_stream);
        }
    }
}

template< class T1, class T2 >
std::list< ISCluster > recursiveParseGraph_SNAP(T1 operation1,
						T2 operation2,
						::SubGraph_SNAP& root,
						ofstream& out_dot_stream,
						const int level,
						const bool approx){
    if (root.subgraphs == NULL && !root.filtered) {
        return operation1(root, out_dot_stream);
    } else {
        out_dot_stream << "subgraph cluster_" << root.id << " {\n";
        out_dot_stream << "style=\"rounded, filled\";\n";
        out_dot_stream << "color=" << level << ";\n";
        //INFO("Parent: " << root.id);
        std::list< ISCluster > clu;
        int new_lev = level - 1;
        for (int i=0; i < root.num_subgraphs; ++i) {
            std::list< ISCluster > childl = recursiveParseGraph_SNAP(operation1,
								     operation2,
								     root.subgraphs[i],
								     out_dot_stream,
								     new_lev,
								     approx);
            clu.splice(clu.begin(), childl);
        }
	INFO("Cluster: " << root.id);
        return operation2(clu, out_dot_stream, approx);
    }
}

struct createInitClusters_SNAP {
    const std::map< string, Alignments >& seq_aln;
    const std::map< string, string >& cons_map;
    createInitClusters_SNAP(const std::map< string, Alignments >& aln,
			    const std::map< string, string >& _cons_map) :
        seq_aln(aln),
	cons_map(_cons_map) {}

    std::list< ISCluster > operator() (::SubGraph_SNAP& g, ofstream& out_dot_stream) {
        if (seq_aln.find(g.id) == seq_aln.end()) {
            out_dot_stream << g.id << "[style=filled, color=gold, label=\"\", width="
			   << log10(g.weight) + 1.0 << "]" << std::endl;
            //INFO("Subg. " << g.id << " has no valid alignments.");
            return std::list< ISCluster >();
        }
	Alignments al = seq_aln.at(g.id);
	for (std::list< AlnData >::iterator it = al.al_data.begin();
	     it != al.al_data.end(); ++it) {
	    std::list< AlnData >::iterator un_it = it;
	    un_it++;
	    for(;un_it != al.al_data.end();) {
		if(un_it->target_id == it->target_id &&
		   un_it->query_strand == it->query_strand &&
		   un_it->target_strand == it->target_strand) {
		    if (it->target_strand == it->query_strand) {
			if(un_it->target_start == it->target_start) {
			    if(un_it->score > it->score) {
				it->score = un_it->score;
			    }
			    un_it = al.al_data.erase(un_it);
			} else {
			    un_it++;
			}
		    } else {
			if(un_it->target_stop == it->target_stop) {
			    if(un_it->score > it->score) {
				it->score = un_it->score;
			    }
			    un_it = al.al_data.erase(un_it);
			} else {
			    un_it++;
			}
		    }
		} else {
		    un_it++;
		}
	    }
        }
        ISCluster c(g.id, g.weight, al);
	c.cons_seq = cons_map.at(g.id);
	c.seq_len = cons_map.at(g.id).length();
        //INFO(c);
        out_dot_stream << g.id << "[style=filled, color=blue, label=\"\", width="
		       << log10(g.weight) + 1.0 << "]" << std::endl;
        return std::list< ISCluster >(1, c);
    }
};

struct mergeClusters {
    std::list< ISCluster >operator() (std::list< ISCluster >& in_clu,
				      ofstream& out_dot_stream,
				      const bool approx) {
	int n_merge = 0;
	do {
	    n_merge = refineClusters(in_clu, out_dot_stream);
	} while(n_merge > 0 && !approx);
        out_dot_stream << "}" << std::endl;
        return in_clu;
    }
};

std::list< ISCluster >
CreateClusters::initialClusters_SNAP(::SubGraph_SNAP& g,
				     const std::map< string, Alignments >& seq_aln,
				     const std::map< string, string >& cons_map,
				     ofstream& out_bed_stream,
				     ofstream& out_dot_stream,
				     const bool approx) {
    out_dot_stream << "strict graph G {\n";
    out_dot_stream << "colorscheme=pastel13;\n";
    out_dot_stream << "outputorder=edgesfirst;\n";
    out_dot_stream << "node [shape=circle];\n";
    std::list< ISCluster > clu = recursiveParseGraph_SNAP(createInitClusters_SNAP(seq_aln,
										  cons_map),
							  mergeClusters(),
							  g,
							  out_dot_stream,
							  3,
							  approx);
    out_dot_stream << "}" << std::endl;

    //Print clusters in BED format
    out_bed_stream << "track name=clusters ";
    out_bed_stream << "description=\"Integration Sites Clusters.\" ";
    out_bed_stream << "useScore=1" << std::endl;
    std::list< ISCluster >::const_iterator clu_it;
    for (clu_it = clu.begin(); clu_it != clu.end(); ++clu_it) {
      out_bed_stream << *clu_it;
    }
    return clu;
}

int CreateClusters::Run(void){
    const CArgs& args = GetArgs();
    assert(args.Exist("i"));
    //Chech input files
    CFile in_asn_file(args["i"].AsString());
    if(!in_asn_file.Exists()) {
	ERROR("File " << args["i"].AsString() << " not found.");
	return 1;
    }
    CFile in_fasta_file(args["f"].AsString());
    if(!in_fasta_file.Exists()) {
        ERROR("File " << args["f"].AsString() << " not found.");
        return 1;
    }
    if(args["m"].HasValue()) {
	CFile in_map_file(args["m"].AsString());
	if(!in_map_file.Exists()) {
	    ERROR("File " << args["m"].AsString() << " not found.");
	    return 1;
	}
    }
    int num_threads = 1;
    num_threads = args["num_threads"].AsInteger();
    
    bool approx = false;
    if(args["approx"]) {
	approx = true;
    }
    bool consensus = true;
    if(args["no-consensus"]) {
	INFO("Avoid to compute final consensus sequences.");
	consensus = false;
    }
    //Subggraph directory
    CDirEntry in_subg_file(args["subg"].AsString());
    if(!in_subg_file.Exists()) {
        ERROR("File " << args["subg"].AsString() << " not found.");
	return 1;
    }
    string subg_dir_name(in_subg_file.GetDir() + "subgraphs");
    CDir subg_dir(subg_dir_name);
    if(!subg_dir.Exists()) {
        ERROR("Cannot find subgraph directory: " << subg_dir_name << ".");
	return 1;
    }
    //Bed output
    string outbed = in_subg_file.GetDir() + in_subg_file.GetBase() + ".all.bed";
    ofstream out_bed_stream(outbed.c_str());
    if(!out_bed_stream.is_open()) {
	ERROR("Error in creating " << outbed << " file.");
	return 1;
    }
    //Dot output
    string outdot = in_subg_file.GetDir() + in_subg_file.GetBase() + ".all.dot";
    ofstream out_dot_stream(outdot.c_str());
    if(!out_dot_stream.is_open()) {
	ERROR("Error in creating " << outdot << " file.");
	return 1;
    }
    //Cluster (serialized) output
    string outclu = in_subg_file.GetDir() + in_subg_file.GetBase() + ".all.clu";
    auto_ptr<CObjectOStream>
        out_clu_stream(CObjectOStream::Open(CLU_TYPE, outclu));
    if(!out_clu_stream->InGoodState()) {
	ERROR("Error in creating " << outclu << " file.");
	return 1;
    }

    INFO("PARAMETERS");
    INFO("Input ASN file: " << args["i"].AsString());
    INFO("Input Subg file: " << args["subg"].AsString());
    INFO("Input Consensus Fasta file: " << args["f"].AsString());
    if(args["m"].HasValue()) {
        INFO("Input Map file: " << args["m"].AsString());
    }
    INFO("Input subgraph dir.: " << subg_dir_name);
    INFO("Num. Threads: " << num_threads);
    INFO("Output BED file: " << outbed);
    INFO("Output DOT file: " << outdot);
    INFO("Output CLU file: " << outclu);

    INFO("Started reading input files.");
    auto_ptr< CObjectIStream > in_asn_stream
        (CObjectIStream::Open(ASN_TYPE, args["i"].AsString()));
    if(!in_asn_stream->InGoodState()) {
	ERROR("Error in opening " << args["i"].AsString() << " file.");
	return 1;
    }

    //Read BLAST alignments
    std::map< string, Alignments > seq_aln;
    while(!in_asn_stream->EndOfData()) {
        Alignments al;
        *in_asn_stream >> al;
        seq_aln[al.query_id] = al;
    }

    //Read serialized SubGraph structure
    ::SubGraph_SNAP g;
    ifstream in_subg_stream(args["subg"].AsString().c_str());
    if(!in_subg_stream.is_open()) {
	ERROR("Error in opening " << args["subg"].AsString() << " file.");
	return 1;
    }
    readGraph_SNAP(g, in_subg_stream);
    in_subg_stream.close();

    //Read consensus sequences
    std::map< string, string > cons_map;
    try {
        gzFile fp = gzopen(args["f"].AsString().c_str(), "r");
        kseq_t* seq = kseq_init(fp);
        while((kseq_read(seq)) >= 0) {
            cons_map[seq->name.s] = seq->seq.s;
        }
        gzclose(fp);
    } catch(exception& e) {
	ERROR("Error in reading consensus sequences: " << e.what());
	return 1;
    }

    //Read serialized FASTA Id Map
    UniqueSeqs unique_seq_map;
    if(args["m"].HasValue()) {
        auto_ptr< CObjectIStream > in_map_stream
            (CObjectIStream::Open(ASN_TYPE, args["m"].AsString()));
        *in_map_stream >> unique_seq_map;
    }

    INFO("Finished reading input files.");

    //Create clusters
    INFO("Started creating clusters.");
    std::list< ISCluster > clu = initialClusters_SNAP(g,
						      seq_aln,
						      cons_map,
						      out_bed_stream,
						      out_dot_stream,
						      approx);
    INFO("Finished creating clusters.");

    if(approx) {
	//Refine clusters in approx mode
	INFO("Start refining computed clusters.");
	refineClusters(clu, out_dot_stream);
	INFO("Finished refining computed clusters.");
    }

    //Compute Simple Score for clusters
    INFO("Start computing simple scores.");
    computeSimpleClusterScore(clu);
    INFO("Finished computing simple scores.");

    if(consensus) {
	//Compute consensus sequences for clusters
	INFO("Start computing new consensus sequences.");
	if(computeClusterConsensus(clu,
				   unique_seq_map.fasta_id_map,
				   cons_map,
				   subg_dir_name,
				   num_threads) == 1) {
	    ERROR("Consensus error.");
	    return 1;
	}
	INFO("Finished computing new consensus sequences.");
    }

    //Serialize the output
    std::list< ISCluster >::iterator clu_it;
    for(clu_it = clu.begin(); clu_it != clu.end(); ++clu_it) {
        *out_clu_stream << *clu_it;
    }
    //Flush and Close
    out_bed_stream.flush();
    out_bed_stream.close();
    out_dot_stream.flush();
    out_dot_stream.close();
    out_clu_stream->Flush();
    return 0;
}

void CreateClusters::Exit(){

}

int main(int argc, char* argv[]){
    INFO("Program started");
    CreateClusters app;
    return app.AppMain(argc, argv);
}
