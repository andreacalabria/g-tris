/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#include "configuration.hpp"
#include "score_comp.hpp"
#include "uniqueseqs.hpp"
#include "iscluster.hpp"
#include "consensus.hpp"

#include <serial/serial.hpp>
#include <serial/objostr.hpp>
#include <serial/objistr.hpp>

#include <sstream>
#include <omp.h>

USING_NCBI_SCOPE;
USING_SCOPE(objects);

struct scoreElem {
    ScoreComputation::TScore sc;
    int n_seq_1;
    int n_seq_2;

    scoreElem(const ScoreComputation::TScore& _sc,
              const int _n_seq_1,
              const int _n_seq_2) :
        sc(_sc),
        n_seq_1(_n_seq_1),
        n_seq_2(_n_seq_2) {}
};

class AnalyzeClusters : public CNcbiApplication {
    virtual void Init(void);
    virtual int Run(void);
    // virtual void Exit();
    std::list< scoreElem > statAnalysis(const ISCluster& c1,
                                        const ISCluster& c2,
                                        const string& subg_dir_name,
                                        const std::map< string,
                                                        std::list< string > >& fasta_id_map,
                                        ScoreComputation& sc,
                                        const double init_cov,
                                        const double init_freq,
                                        const double med_cov,
                                        const double med_freq,
                                        const double high_cov,
                                        const double high_freq,
                                        const bool print);
										
    std::list< scoreElem > bigStatAnalysis(const ISCluster& c1,
                                        const ISCluster& c2,
                                        const string& subg_dir_name,
                                        const std::map< string,
                                                        std::list< string > >& fasta_id_map,
                                        const double init_cov,
                                        const double init_freq,
                                        const double med_cov,
                                        const double med_freq,
                                        const double high_cov,
                                        const double high_freq,
                                        const bool print);
};

void AnalyzeClusters::Init(void) {
    auto_ptr<CArgDescriptions> arg_desc(new CArgDescriptions);
    // Specify USAGE context
    CNcbiApplication::DisableArgDescriptions();

    arg_desc->SetUsageContext(GetArguments().GetProgramBasename(),
                              "Cluster Analyze program");

    arg_desc->AddKey
        ("i", "inputfile",
         "Input file of serialized clusters (.clu).",
         CArgDescriptions::eInputFile, CArgDescriptions::fPreOpen);

    arg_desc->AddOptionalKey
        ("m", "mapfile",
         "File containing the map of unique sequences " \
         "obtained with option '-uniqueseqs' of "       \
         "makeblastdb program.",
         CArgDescriptions::eInputFile);

    arg_desc->AddDefaultKey
        ("num_threads", "num_threads",
         "Number of threads used to analyze clusters. Default: 1.",
         CArgDescriptions::eInteger, "1");

    arg_desc->AddOptionalKey
        ("init_cov", "init_cov",
         "Consensus beginning sequence coverage threshold " \
         "(INITIAL SEQUENCE CONVERAGE). Default: 0.75",
         CArgDescriptions::eDouble);

    arg_desc->AddOptionalKey
        ("init_freq", "init_freq",
         "Consensus beginning symbol similarity threshold " \
         "(INTIAL SYMBOL FREQUENCY). Default: 0.75",
         CArgDescriptions::eDouble);

    arg_desc->AddOptionalKey
        ("med_cov", "med_cov",
         "Consensus medium ending sequence coverage threshold " \
         "(MEDIUM SEQUENCE CONVERAGE). Default: 0.75",
         CArgDescriptions::eDouble);

    arg_desc->AddOptionalKey
        ("med_freq", "med_freq",
         "Consensus medium ending symbol similarity threshold " \
         "(MEDIUM SYMBOL FREQUENCY). Default: 0.75",
         CArgDescriptions::eDouble);

    arg_desc->AddOptionalKey
        ("high_cov", "high_cov",
         "Consensus high ending sequence coverage threshold " \
         "(HIGH SEQUENCE CONVERAGE). Default: 0.75",
         CArgDescriptions::eDouble);

    arg_desc->AddOptionalKey
        ("high_freq", "high_freq",
         "Consensus high ending symbol similarity threshold " \
         "(HIGH SYMBOL FREQUENCY). Default: 0.75",
         CArgDescriptions::eDouble);

    // Setup arg.descriptions for this application
    SetupArgDescriptions(arg_desc.release());
}

class Generator {
public:
    Generator() {};
    virtual bool has_next() = 0;
    virtual std::vector< bool > next() = 0;
};

class RandGenerator : public Generator {
private:
    int n_gen;
    int max_gen;
    std::vector< bool > v;
public:
    RandGenerator(const int _vec_len,
		  const int _max_gen = PERM_TEST_RAND_GENERATIONS) :
        n_gen(0),
        max_gen(_max_gen),
        v(_vec_len, false) {}

    virtual bool has_next() {
        return n_gen < max_gen;
    }

    virtual std::vector< bool > next() {
        ++n_gen;
        v[0] = false;
        unsigned int pos = rand() % (v.size() - 2) + 1;
        v[pos] = true;
        for (unsigned int i = 1; i < v.size(); ++i) {
            if (i != pos) {
                double r = (double) rand() / (RAND_MAX);
                if (r <= 0.5) {
                    v[i] = true;
                } else {
                    v[i] = false;
                }
            }
        }
        return v;
    }
};

class FullGenerator : public Generator {
private:
    int n_gen;
    std::vector< bool > v;
public:
    FullGenerator(const int _vec_len) :
        n_gen(0),
        v(_vec_len, false) {}

    virtual bool has_next() {
        return n_gen < (1 << (v.size() - 1)) -1;
    }

    virtual std::vector< bool > next() {
        ++n_gen;
        std::vector< bool >::reverse_iterator rit = v.rbegin();
        while(*rit && rit != v.rend()) {
            *rit = false;
            ++rit;
        }
        *rit = true;
        return v;
    }
};

template < typename MultipleSequenceAligner >
    struct CreateConsensusStruct {
    std::map< string,
              std::list< string > >& fasta_id_map;
    const MultipleSequenceAligner& msa;
    const double init_cov;
    const double init_freq;
    const double med_cov;
    const double med_freq;
    const double high_cov;
    const double high_freq;
    const bool print;

    CreateConsensusStruct(std::map< string,
                                    std::list< string > >& _fasta_id_map,
                          const MultipleSequenceAligner& _msa,
                          const double _init_cov,
                          const double _init_freq,
                          const double _med_cov,
                          const double _med_freq,
                          const double _high_cov,
                          const double _high_freq,
                          const bool _print) :
        fasta_id_map(_fasta_id_map),
        msa(_msa),
        init_cov(_init_cov),
        init_freq(_init_freq),
        med_cov(_med_cov),
        med_freq(_med_freq),
        high_cov(_high_cov),
        high_freq(_high_freq),
        print(_print)
    {}

    string computeConsensus(const string& fname,
                            const int num_seq) {
        string consensus = "";
        if (num_seq == 1) {
            try{
                // std::cout << "Single sequence. No ClustalW alignment.\n";
                gzFile fp = gzopen(fname.c_str(), "r");
                kseq_t* seq = kseq_init(fp);
                kseq_read(seq);
                string s = seq->seq.s;
                gzclose(fp);
                // std::cout << s << "\t";
                // std::cout << s.length() << "\t";
                // std::cout << "\n";
                consensus = s;
            } catch(exception& e) {
                ERROR("Fasta file error: " << e.what());
            }
        } else {
            consensus = representativeString(fasta_id_map,
                                             fname,
                                             msa,
                                             init_cov,
                                             init_freq,
                                             med_cov,
                                             med_freq,
                                             high_cov,
                                             high_freq,
                                             print);
        }
        return consensus;
    }
};

template<class fwditer>
fwditer random_unique(fwditer begin, fwditer end, size_t num_random) {
    size_t left = std::distance(begin, end);
    while(num_random--) {
        fwditer r = begin;
        std::advance(r, rand()%left);
        std::swap(*begin, *r);
        ++begin;
        --left;
    }
    return begin;
}

int prepareStatFiles(const ISCluster& c1,
		     const ISCluster& c2,
		     const string& subg_dir_name,
		     const std::map< string, std::list< string > >& fasta_id_map,
		     const stringstream& omp_pre,
		     std::vector< std::pair< string, string > >& tmp_seq,
		     std::map< string, std::list< string > >& tmp_fasta_id_map,
			 int& num_unique_seq) {
    //Crete FASTA file with all original sequences of c1 and c2
    string tmpoutfasta(subg_dir_name + "/" + omp_pre.str() + "statAnalysis.fa");
    std::ofstream tmp_out_fasta_stream(tmpoutfasta.c_str(),
				       std::ios_base::binary);
    if(!tmp_out_fasta_stream.is_open()) {
	ERROR("Error in creating " << tmpoutfasta << " file.");
	return 0;
    }
    string infastac1(subg_dir_name + "/" + c1.clu_id + ".fa");
    std::ifstream in_fasta_c1_stream(infastac1.c_str(),
				     std::ios_base::binary);
    if(!in_fasta_c1_stream.is_open()) {
	ERROR("Error in opening " << infastac1 << " file.");
	return 0;
    }
    string infastac2(subg_dir_name + "/" + c2.clu_id + ".fa");
    std::ifstream in_fasta_c2_stream(infastac2.c_str(),
				     std::ios_base::binary);
    if(!in_fasta_c2_stream.is_open()) {
	ERROR("Error in opening " << infastac2 << " file.");
	return 0;
    }
    tmp_out_fasta_stream << in_fasta_c1_stream.rdbuf() << in_fasta_c2_stream.rdbuf();
    tmp_out_fasta_stream.close();
    in_fasta_c1_stream.close();
    in_fasta_c2_stream.close();
	
    int num_seq = 0;
	num_unique_seq = 0;
    try{
        gzFile fp = gzopen(tmpoutfasta.c_str(), "r");
        kseq_t* seq = kseq_init(fp);
        while ((kseq_read(seq)) >= 0) {
            //Unique read
            ++num_seq;
			++num_unique_seq;
            tmp_seq.push_back(make_pair(seq->name.s, seq->seq.s));
            tmp_fasta_id_map[seq->name.s];
            //Other reads having the same sequence
            std::list< string >::const_iterator fasta_id_it;
            for (fasta_id_it = fasta_id_map.at(seq->name.s).begin();
                 fasta_id_it != fasta_id_map.at(seq->name.s).end();
                 ++fasta_id_it) {
                ++num_seq;
                tmp_seq.push_back(make_pair(*fasta_id_it, seq->seq.s));
                tmp_fasta_id_map[*fasta_id_it];
            }
        }
        gzclose(fp);
    } catch(exception& e) {
        ERROR("StatAnalysis file error: " << e.what());
	return 0;
    }
    return num_seq;
}

std::list< scoreElem >
AnalyzeClusters::bigStatAnalysis(const ISCluster& c1,
				 const ISCluster& c2,
				 const string& subg_dir_name,
				 const std::map< string, std::list< string > >& fasta_id_map,
				 const double init_cov,
				 const double init_freq,
				 const double med_cov,
				 const double med_freq,
				 const double high_cov,
				 const double high_freq,
				 const bool print) {
    std::list< scoreElem > scoreList;
	
    //Multiple sequence alignment and consensus
    std::string bin_path= "";
    char* _bin_path= readlink_malloc("/proc/self/exe");
    if (_bin_path==NULL) {
        ERROR("Impossible to determine the path of the executable. " <<
              "Let it unspecified (i.e., search in the PATH).");
	return scoreList;
    } else {
        bin_path = _bin_path;
        free(_bin_path);
        CDirEntry cd(bin_path);
        bin_path = cd.GetDir();
    }
    MultipleSequenceAligner_ClustalW msa_clustalw(bin_path);
    stringstream omp_pre;
	
    std::vector< std::pair< string, string > > tmp_seq;
    std::map< string, std::list< string > > tmp_fasta_id_map;
	int num_unique_seq = 0;
    int num_seq = prepareStatFiles(c1,
				   c2,
				   subg_dir_name, 
				   fasta_id_map, 
				   omp_pre, tmp_seq, 
				   tmp_fasta_id_map,
				   num_unique_seq);
    if(num_seq == 0) {
	return scoreList;
    }
    std::cout << "Num. Sequences: " << num_seq << "\n";
    std::cout << "Num. Unique Sequences: " << num_unique_seq << "\n";
  
    //if(num_seq > 300) return scoreList;

    try{
        //Create consensus structure
        CreateConsensusStruct< MultipleSequenceAligner_ClustalW >
            consStruct(tmp_fasta_id_map,
                       msa_clustalw,
                       init_cov,
                       init_freq,
                       med_cov,
                       med_freq,
                       high_cov,
                       high_freq,
                       print);
	int num_test_iter = num_unique_seq / (MAX_SEQ_BIG_STAT_THR * 2) + 1;
	std::cout << "Num. Statistics Iteration(s): " << num_test_iter << std::endl;
	for(int iter = 0; iter < num_test_iter; ++iter) {
	    std::vector< std::pair< string, string > > tmp_seq_test;
	    if(num_seq < MAX_SEQ_BIG_STAT_THR) {
		std::cout << "Select all sequences: " << num_seq << std::endl;
		tmp_seq_test = tmp_seq;
	    } else {
		random_unique(tmp_seq.begin(), 
			      tmp_seq.end(), 
			      MAX_SEQ_BIG_STAT_THR);
		tmp_seq_test.insert(tmp_seq_test.begin(), 
				    tmp_seq.begin(), 
				    tmp_seq.begin() + MAX_SEQ_BIG_STAT_THR);
		std::cout << "Select random sequences: " << tmp_seq_test.size() << std::endl;
	    }
	    //Stat Analysis
	    int num_generation = 0;
	    ScoreComputation sc;
	    bool generate = true;
	    Generator* g = new RandGenerator(tmp_seq_test.size());
	    int thread_num;
#pragma omp parallel private(sc, thread_num)
	    {
		while (generate) {
		    stringstream omp_pre_str;
		    std::vector< bool > v;      	
#pragma omp critical(generator)
		    {
			generate = g->has_next();
			v = g->next();
			thread_num = omp_get_thread_num();
			num_generation++;
			// std::cout << "Num. Gen.: " << num_generation << std::endl;
		    }
#pragma omp flush(generate, num_generation)
#ifdef ENABLE_OPENMP
		    omp_pre_str << thread_num;
#endif
		    string fname1(subg_dir_name + "/" +
				  omp_pre_str.str() + "bigStatAnalysisFile1.fa");
		    string fname2(subg_dir_name + "/" +
				  omp_pre_str.str() + "bigStatAnalysisFile2.fa");
		    std::ofstream f1(fname1.c_str(), std::ios_base::binary);
		    if(!f1.is_open()) {
			ERROR("Error in creating " << fname1 << " file.");
			continue;
		    }
		    std::ofstream f2(fname2.c_str(), std::ios_base::binary);
		    if(!f2.is_open()) {
			ERROR("Error in creating " << fname2 << " file.");
			continue;
		    }
		    int n_seq1 = 0;
		    int n_seq2 = 0;

		    for (unsigned int i = 0; i < tmp_seq_test.size(); ++i) {
                	if (v[i]) {
			    ++n_seq1;
			    f1 << ">" << tmp_seq_test[i].first << "\n";
			    f1 << tmp_seq_test[i].second << "\n";
                	} else {
			    ++n_seq2;
			    f2 << ">" << tmp_seq_test[i].first << "\n";
			    f2 << tmp_seq_test[i].second << "\n";
                	}
		    }
		    f1.close();
		    f2.close();
		    if (n_seq1 == 0 || n_seq2 == 0) continue;
		    string cons1 = consStruct.computeConsensus(fname1,
							       n_seq1);
		    string cons2 = consStruct.computeConsensus(fname2,
							       n_seq2);
		    scoreElem sc_el = scoreElem(sc.compute_score(cons1, cons2),
						n_seq1,
						n_seq2);
#pragma omp critical(generator)
		    {
			scoreList.push_back(sc_el);
		    }
		    remove(fname1.c_str());
		    remove(fname2.c_str());
        	}
	    }
	}
    } catch(exception& e) {
        ERROR("Big StatAnalysis file error: " << e.what());
    }
    return scoreList;
}


std::list< scoreElem >
AnalyzeClusters::statAnalysis(const ISCluster& c1,
			      const ISCluster& c2,
			      const string& subg_dir_name,
			      const std::map< string, std::list< string > >& fasta_id_map,
			      ScoreComputation& sc,
			      const double init_cov,
			      const double init_freq,
			      const double med_cov,
			      const double med_freq,
			      const double high_cov,
			      const double high_freq,
			      const bool print) {
    std::list< scoreElem > scoreList;
	
    //Multiple sequence alignment and consensus
    std::string bin_path= "";
    char* _bin_path= readlink_malloc("/proc/self/exe");
    if (_bin_path==NULL) {
        ERROR("Impossible to determine the path of the executable. " <<
              "Let it unspecified (i.e., search in the PATH).");
	return scoreList;
    } else {
        bin_path = _bin_path;
        free(_bin_path);
        CDirEntry cd(bin_path);
        bin_path = cd.GetDir();
    }
    MultipleSequenceAligner_ClustalW msa_clustalw(bin_path);
    stringstream omp_pre;
#ifdef ENABLE_OPENMP
    omp_pre << omp_get_thread_num();
#endif

    // string tmpoutfasta(subg_dir_name + "/" + omp_pre.str() + "statAnalysis.fa");
    // std::ofstream tmp_out_fasta_stream(tmpoutfasta.c_str(),
    // 				       std::ios_base::binary);
    // if(!tmp_out_fasta_stream.is_open()) {
    // 	ERROR("Error in creating " << tmpoutfasta << " file.");
    // 	return scoreList;
    // }
    // string infastac1(subg_dir_name + "/c_" + c1.clu_id + ".fa");
    // std::ifstream in_fasta_c1_stream(infastac1.c_str(),
    // 				     std::ios_base::binary);
    // if(!in_fasta_c1_stream.is_open()) {
    // 	ERROR("Error in opening " << infastac1 << " file.");
    // 	return scoreList;
    // }
    // string infastac2(subg_dir_name + "/c_" + c2.clu_id + ".fa");
    // std::ifstream in_fasta_c2_stream(infastac2.c_str(),
    // 				     std::ios_base::binary);
    // if(!in_fasta_c2_stream.is_open()) {
    // 	ERROR("Error in opening " << infastac2 << " file.");
    // 	return scoreList;
    // }
    // tmp_out_fasta_stream << in_fasta_c1_stream.rdbuf() << in_fasta_c2_stream.rdbuf();
    // tmp_out_fasta_stream.close();
    // in_fasta_c1_stream.close();
    // in_fasta_c2_stream.close();

    //Crete FASTA file with all original sequences of c1 and c2	
    std::vector< std::pair< string, string > > tmp_seq;
    std::map< string, std::list< string > > tmp_fasta_id_map;
	int num_unique_seq = 0;
    int num_seq = prepareStatFiles(c1, 
				   c2, 
				   subg_dir_name, 
				   fasta_id_map, 
				   omp_pre, tmp_seq, 
				   tmp_fasta_id_map,
				   num_unique_seq);
    //Ignore empty and big statistics
    if(num_seq == 0 || num_seq > MAX_SEQ_SMALL_STAT_THR) {
	return scoreList;
    }
    // Cerate a random partition
    // int num_seq = 0;
    try{
        // gzFile fp = gzopen(tmpoutfasta.c_str(), "r");
        // kseq_t* seq = kseq_init(fp);
        // std::vector< std::pair< string, string > > tmp_seq;
        // std::map< string, std::list< string > > tmp_fasta_id_map;
        // while ((kseq_read(seq)) >= 0) {
        //     //Unique read
        //     ++num_seq;
        //     tmp_seq.push_back(make_pair(seq->name.s, seq->seq.s));
        //     tmp_fasta_id_map[seq->name.s];
        //     //Other reads having the same sequence
        //     std::list< string >::const_iterator fasta_id_it;
        //     for (fasta_id_it = fasta_id_map.at(seq->name.s).begin();
        //          fasta_id_it != fasta_id_map.at(seq->name.s).end();
        //          ++fasta_id_it) {
        //         ++num_seq;
        //         tmp_seq.push_back(make_pair(*fasta_id_it, seq->seq.s));
        //         tmp_fasta_id_map[*fasta_id_it];
        //     }
        // }
        // gzclose(fp);

	// if(num_seq > 10) {
	//     //INFO("Big statistics.");
	//     return scoreList;
	// }


        // Create consensus structure
        CreateConsensusStruct< MultipleSequenceAligner_ClustalW >
            consStruct(tmp_fasta_id_map,
                       msa_clustalw,
                       init_cov,
                       init_freq,
                       med_cov,
                       med_freq,
                       high_cov,
                       high_freq,
                       print);

        //Stat Analysis
	Generator* g = new FullGenerator(tmp_seq.size());
        
	// Generator* g = NULL;
        // if (num_seq > 10) {
	//     // INFO("Random Test Generator.");
	//     g = new RandGenerator(tmp_seq.size());
        // } else {
	//     // INFO("Full Test Generator.");
	//     g = new FullGenerator(tmp_seq.size());
        // }

        string fname1(subg_dir_name + "/" +
                      omp_pre.str() + "statAnalysisFile1.fa");
        string fname2(subg_dir_name + "/" +
                      omp_pre.str() + "statAnalysisFile2.fa");
        while (g->has_next()) {
            remove(fname1.c_str());
            remove(fname2.c_str());
            std::vector< bool > v = g->next();
            std::ofstream f1(fname1.c_str(), std::ios_base::binary);
	    if(!f1.is_open()) {
		ERROR("Error in creating " << fname1 << " file.");
		continue;
	    }
            std::ofstream f2(fname2.c_str(), std::ios_base::binary);
	    if(!f2.is_open()) {
		ERROR("Error in creating " << fname2 << " file.");
		continue;
	    }
            int n_seq1 = 0;
            int n_seq2 = 0;
	    
            for (unsigned int i = 0; i < tmp_seq.size(); ++i) {
                if (v[i]) {
                    ++n_seq1;
                    f1 << ">" << tmp_seq[i].first << "\n";
                    f1 << tmp_seq[i].second << "\n";
                } else {
                    ++n_seq2;
                    f2 << ">" << tmp_seq[i].first << "\n";
                    f2 << tmp_seq[i].second << "\n";
                }
            }
            f1.close();
            f2.close();
            if (n_seq1 == 0 || n_seq2 == 0) continue;
            string cons1 = consStruct.computeConsensus(fname1,
                                                       n_seq1);
            string cons2 = consStruct.computeConsensus(fname2,
                                                       n_seq2);
            scoreList.push_back(scoreElem(sc.compute_score(cons1, cons2),
                                          n_seq1,
                                          n_seq2));
        }
    	remove(fname1.c_str());
    	remove(fname2.c_str());
    } catch(exception& e) {
        ERROR("StatAnalysis file error: " << e.what());
    }
    return scoreList;
}

int AnalyzeClusters::Run(void) {
    std::srand(time(0));
    const CArgs& args = GetArgs();
    assert(args.Exist("i"));

    //Check input files
    CDirEntry in_clu_file(args["i"].AsString());
    if(!in_clu_file.Exists()) {
	ERROR("File " << args["i"].AsString() << " not found.");
	return 1;
    }
    string subg_dir_name(in_clu_file.GetDir() + "subgraphs");
    CDir subg_dir(subg_dir_name);
    if (!subg_dir.Exists()) {
        ERROR("Cannot find subgraph directory: " << subg_dir_name << ".");
	return 1;
    }
    if (args["m"].HasValue()) {
	CFile in_map_file(args["m"].AsString());
	if(!in_map_file.Exists()) {
	    ERROR("File " << args["m"].AsString() << " not found.");
	    return 1;
	}
    }

    //Distribution output
    string outdist = in_clu_file.GetDir() + in_clu_file.GetBase() + ".dist";
    ofstream out_dist_stream(outdist.c_str());
    if(!out_dist_stream.is_open()) {
	ERROR("Error in creating " << outdist << " file.");
	return 1;
    }

    // Print input parameters
    INFO("PARAMETERS");
    INFO("Input CLU file: " << args["i"].AsString());
    if (args["m"].HasValue()) {
        INFO("Input MAP file: " << args["m"].AsString());
    }
    INFO("Input subgraph dir.: " << subg_dir_name);

#ifdef ENABLE_OPENMP
    if (args["num_threads"].AsInteger() > 1) {
        if (args["num_threads"].AsInteger() < omp_get_num_procs()) {
            omp_set_num_threads(args["num_threads"].AsInteger());
            INFO("N. Threads: " << args["num_threads"].AsInteger());
        } else {
            omp_set_num_threads(omp_get_num_procs());
            INFO("N. Threads: " << omp_get_num_procs());
        }
    } else {
        INFO("N. Threads: 1");
    }
#endif

    INFO("Output DIST file: " << outdist);
    INFO("Started reading input files.");
    std::vector< ISCluster > clu;
    {
	auto_ptr< CObjectIStream > in_clu_stream
	    (CObjectIStream::Open(CLU_TYPE, args["i"].AsString()));
	if(!in_clu_stream->InGoodState()) {
	    ERROR("Error in opening " << args["i"].AsString() << " file.");
	    return 1;
	}
	while (!in_clu_stream->EndOfData()) {
	    ISCluster c;
	    *in_clu_stream >> c;
	    clu.push_back(c);
	}
    }
    //Read serialized FASTA Id Map
    UniqueSeqs unique_seq_map;
    {{
	if (args["m"].HasValue()) {
	    auto_ptr< CObjectIStream > in_map_stream
		(CObjectIStream::Open(ASN_TYPE, args["m"].AsString()));
	    if(!in_map_stream->InGoodState()) {
		ERROR("Error in opening " << args["m"].AsString() << " file.");
		return 1;
	    }
	    *in_map_stream >> unique_seq_map;
	}
    }}
    INFO("Finished reading input files.");
    
    INFO("Num. Read Clusters: " << clu.size());

    //Set consensus thresholds
    double init_cov = INIT_W_SEQ_THR;
    if (args["init_cov"].HasValue()) {
	init_cov = args["init_cov"].AsDouble();
    }
    double init_freq = INIT_W_SYM_THR;;
    if (args["init_freq"].HasValue()) {
	init_freq = args["init_freq"].AsDouble();
    }
    double med_cov = END_MED_W_SEQ_THR;
    if (args["med_cov"].HasValue()) {
	med_cov = args["med_cov"].AsDouble();
    }
    double med_freq = END_MED_W_SYM_THR;
    if (args["med_freq"].HasValue()) {
	med_freq = args["med_freq"].AsDouble();
    }
    double high_cov = END_HIGH_W_SEQ_THR;
    if (args["high_cov"].HasValue()) {
	high_cov = args["high_cov"].AsDouble();
    }
    double high_freq = END_HIGH_W_SYM_THR;
    if (args["high_freq"].HasValue()) {
	high_freq = args["high_freq"].AsDouble();
    }

    //Generate matrix of consensus alignments
    INFO("Start computing bit_score consensus alignment matrix.");
	
    // for(unsigned int i=0; i < clu.size(); ++i) {
    // 	std::cout << "CLU\t" << clu.at(i).clu_id << "\n";
    // }
	
    //Big statistics
    std::vector< std::pair< unsigned int, unsigned int > > big_stat;
    int num_stat = 0;
    int small_stat = 0;
    int good = 0;
    int bad = 0;
    int not_r = 0;
    ScoreComputation sc;
    unsigned int num_iterations = clu.size() * (clu.size() - 1) / 2;
    unsigned int off1 = 0;
    unsigned int off2 = off1 + 1;
#pragma omp parallel for schedule(dynamic, 1) shared(num_stat, good, bad, not_r, off1, off2, big_stat) private(sc)
    for (unsigned int i = 0; i < num_iterations; ++i) {
	int c1_off = 0;
	int c2_off = 0;
#pragma omp critical
	{
	    if(off2 == clu.size()) {
		off1++;
		off2 = off1 + 1;
	    }
	    c1_off = off1;
	    c2_off = off2;
	    off2++;
	}
	ISCluster c1 = clu.at(c1_off);
	//c1 += c1_off;
	ISCluster c2 = clu.at(c2_off);
	//c2 += c2_off;
        
        ScoreComputation::TScore score;
        score = sc.compute_score(c1.cons_seq, c2.cons_seq);
#pragma omp critical
	{
	    std::cout << "******************************************\n"
		      << " ORIGINAL: "
		      << c1.clu_id << " - " << c2.clu_id << "\n"
		      << "C1 Seq.: " << c1.weight
		      << " C2 Seq.: " << c2.weight << "\n"
		      // << score.aln
		      << "Bitscore: " << score.bit_score << "\n"
		      << "Score: " << (score.bit_score/score.aln_len) << "\n"
		      << "******************************************\n"
		      << std::endl;
	}

	//Condition to perform statAnalysis
	if ((score.bit_score > MIN_BITSCORE_THR ||
	     (score.bit_score/score.aln_len) > MIN_BITS_ALNLEN_THR) &&
	    (score.bit_score/score.aln_len) > MIN_ABS_BITS_ALNLEN_THR) {
	    if (c1.weight > 1 || c2.weight > 1) {
		
		std::list< scoreElem > scoreList;
		scoreList = statAnalysis(c1,
					 c2,
					 subg_dir_name,
					 unique_seq_map.fasta_id_map,
					 sc,
					 init_cov,
					 init_freq,
					 med_cov,
					 med_freq,
					 high_cov,
					 high_freq,
					 false);

#pragma omp critical
		{
		    ++num_stat;
                    if (!scoreList.empty()) {
			small_stat++;
			std::cout << "Small Statistics: " << scoreList.size() << " elements. \n";
			//Chek Results
                    	if (scoreList.size() > RELEVANT_STAT_THR) {
			    //Count number of statistically worse and better
			    double st_better = 0;
			    double st_worse = 0;
			    std::list< scoreElem >::iterator scel_it;
			    for (scel_it = scoreList.begin();
				 scel_it != scoreList.end(); ++scel_it) {
				//Promote to worse the ones having bitscore
				//equal to the minimum possible value
				if (scel_it->sc.bit_score <= MIN_BITSCORE_VAL) {
				    scel_it->sc.bit_score = score.bit_score + 1;
				}
				if (scel_it->sc.bit_score < score.bit_score) {
				    ++st_better;
				} else {
				    ++st_worse;
				}
			    }
			    //Check if outlier
			    if (st_better / (st_better + st_worse) <= OUTLIER_THR) {
                            	++good;
                            	INFO(c1.clu_id << " - "
				     << c2.clu_id << "\t"
				     << "Good statistic.");
			    } else {
                            	++bad;
                            	INFO(c1.clu_id << " - "
				     << c2.clu_id << "\t"
				     << "Bad statistic.");
			    }
                    	} else {
			    ++not_r;
			    WARN(c1.clu_id << " - "
				 << c2.clu_id << "\t"
				 << "Not relevant statistic.");
                    	}

                        //Distribution output
                        out_dist_stream << c1.clu_id << "-" << c2.clu_id << "\t"
					<< score.bit_score << "\t"
					<< (score.bit_score/score.aln_len) << "\t"
					<< "O" << "\n";
                        int num_iter = 0;
                        std::list< scoreElem >::const_iterator sc_it;
                        for (sc_it = scoreList.begin();
                             sc_it != scoreList.end(); ++sc_it) {
                            ++num_iter;
                            //std::cout << "******************************************\n"
                            //          << " ITERATION " << num_iter << ": "
                            //          << c1->clu_id << " - "
                            //          << c2->clu_id << "\n"
                            //          << "N. Seq1: " << sc_it->n_seq_1
                            //          << " - N. Seq2: " << sc_it->n_seq_2 << "\n"
                            //          << sc_it->sc.aln
                            //          << "Bitscore: "
                            //          << sc_it->sc.bit_score << "\n"
                            //          << "Score: "
                            //          << (sc_it->sc.bit_score/sc_it->sc.aln_len) << "\n"
                            //          << "******************************************\n"
                            //          << "\n";

                            //Distribution output
                            out_dist_stream << c1.clu_id << "-" << c2.clu_id << "\t"
					    << sc_it->sc.bit_score << "\t"
					    << (sc_it->sc.bit_score/sc_it->sc.aln_len) << "\t"
					    << "S" << "\n";
                        }
                    } else {
			big_stat.push_back(make_pair(c1_off, c2_off));
                    	//INFO("Empty ScoreList.");
                    }
		}
	    }
	}
    }
    INFO("Processed statistics: " << small_stat);
    INFO("Missing (big) statistics: " << big_stat.size());
    //Process big statistics
    INFO("Start computing big statistics.");
    for(unsigned int i = 0; i < big_stat.size(); ++i) {
	ISCluster c1 = clu.at(big_stat.at(i).first);
	ISCluster c2 = clu.at(big_stat.at(i).second);
	ScoreComputation::TScore score;
        score = sc.compute_score(c1.cons_seq, c2.cons_seq);
        std::list< scoreElem > scoreList;
        scoreList = bigStatAnalysis(c1,
				    c2,
				    subg_dir_name,
				    unique_seq_map.fasta_id_map,
				    init_cov,
				    init_freq,
				    med_cov,
				    med_freq,
				    high_cov,
				    high_freq,
				    false);
	if(!scoreList.empty()) {
	    std::cout << "Big Statistics: " << scoreList.size() << " elements. \n";
	    //Chek Results
	    if (scoreList.size() > RELEVANT_STAT_THR) {
		//Count number of statistically worse and better
		double st_better = 0;
		double st_worse = 0;
		std::list< scoreElem >::iterator scel_it;
		for (scel_it = scoreList.begin();
		     scel_it != scoreList.end(); ++scel_it) {
		    //Promote to worse the ones having bitscore
		    //equal to the minimum possible value
		    if (scel_it->sc.bit_score <= MIN_BITSCORE_VAL) {
			scel_it->sc.bit_score = score.bit_score + 1;
		    }
		    if (scel_it->sc.bit_score < score.bit_score) {
			++st_better;
		    } else {
			++st_worse;
		    }
		}
		//Check if outlier
		if (st_better / (st_better + st_worse) <= OUTLIER_THR) {
		    ++good;
		    INFO(c1.clu_id << " - "
			 << c2.clu_id << "\t"
			 << "Good statistic.");
		} else {
		    ++bad;
		    INFO(c1.clu_id << " - "
			 << c2.clu_id << "\t"
			 << "Bad statistic.");
		}
	    } else {
		++not_r;
		WARN(c1.clu_id << " - "
		     << c2.clu_id << "\t"
		     << "Not relevant statistic.");
	    }
	    
	    //Distribution output
	    out_dist_stream << c1.clu_id << "-" << c2.clu_id << "\t"
			    << score.bit_score << "\t"
			    << (score.bit_score/score.aln_len) << "\t"
			    << "O" << "\n";
	    std::list< scoreElem >::const_iterator sc_it;
	    for (sc_it = scoreList.begin();
		 sc_it != scoreList.end(); ++sc_it) {
		//std::cout << "******************************************\n"
		//          << " ITERATION " << num_iter << ": "
		//          << c1->clu_id << " - "
		//          << c2->clu_id << "\n"
		//          << "N. Seq1: " << sc_it->n_seq_1
		//          << " - N. Seq2: " << sc_it->n_seq_2 << "\n"
		//          << sc_it->sc.aln
		//          << "Bitscore: "
		//          << sc_it->sc.bit_score << "\n"
		//          << "Score: "
		//          << (sc_it->sc.bit_score/sc_it->sc.aln_len) << "\n"
		//          << "******************************************\n"
		//          << "\n";
		
		//Distribution output
		out_dist_stream << c1.clu_id << "-" << c2.clu_id << "\t"
				<< sc_it->sc.bit_score << "\t"
				<< (sc_it->sc.bit_score/sc_it->sc.aln_len) << "\t"
				<< "S" << "\n";
	    }
	} else {
	    WARN("Empty ScoreList.");
	}
    }
    
    out_dist_stream.flush();
    out_dist_stream.close();
    INFO("Finished computing bit_score consensus alignment matrix.");
    INFO("Matrix elements: " << num_iterations);
    INFO("Tot. Elements (statistically) evaluated: " << num_stat);
    INFO("Good Elements: " << good);
    INFO("Bad Elements: " << bad);
    INFO("Not Statistically Relevant Elements: " << not_r);
    INFO("Big Statistics: " << big_stat.size());
    return 0;
}

int main(int argc, char* argv[]){
    INFO("Program started");
    AnalyzeClusters app;
    return app.AppMain(argc, argv);
}
