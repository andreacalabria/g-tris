/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#include "log.hpp"
#include "alignments.hpp"
#include "configuration.hpp"
#include "blast_executer.hpp"

#include <algo/blast/api/local_blast.hpp>
#include <algo/blast/api/objmgr_query_data.hpp>
#include <algo/blast/blastinput/blast_fasta_input.hpp>
#include <algo/blast/api/blast_nucl_options.hpp>

#include <ncbi_pch.hpp>
#include <corelib/ncbiapp.hpp>
#include <algo/blast/api/local_blast.hpp>
#include <algo/blast/api/remote_blast.hpp>
#include <algo/blast/blastinput/blast_fasta_input.hpp>
#include <algo/blast/blastinput/blastn_args.hpp>
#include <algo/blast/api/objmgr_query_data.hpp>
#include <algo/blast/format/blast_format.hpp>

#include <map>
#include <list>
#include <vector>
#include <serial/objistr.hpp>
#include <serial/objostr.hpp>
#include <serial/serialimpl.hpp>
#include <serial/serial.hpp>
#include <serial/iterator.hpp>
#include <objmgr/util/sequence.hpp>

#include <omp.h>
//Kseq fasta parser
#include <zlib.h>
#include <stdio.h>
#include "kseq.h"

KSEQ_INIT(gzFile, gzread)

USING_NCBI_SCOPE;
USING_SCOPE(objects);
USING_SCOPE(blast);

class SeqAlignment : public CNcbiApplication {
    virtual void Init(void);
    virtual int Run(void);
    // virtual void Exit();
};

void SeqAlignment::Init(void){
    auto_ptr<CArgDescriptions> arg_desc(new CArgDescriptions);
    // Specify USAGE context
    CNcbiApplication::DisableArgDescriptions();

    arg_desc->SetUsageContext(GetArguments().GetProgramBasename(),
                              "Sequence Alignment program. Perform BLAST alignment " \
			      "of input sequences against themselves.");

    arg_desc->AddKey
        ("i", "inputfile",
         "Input FASTA file of all the sequences.",
         CArgDescriptions::eInputFile, CArgDescriptions::fPreOpen);

    arg_desc->AddOptionalKey
        ("db", "database",
         "BLAST database name. Default: input FASTA file.",
          CArgDescriptions::eInputFile);

    arg_desc->AddDefaultKey
        ("perc_identity", "perc_identity",
         "Percent identity cutoff.",
         CArgDescriptions::eDouble, BLAST_IDENTITY);

    CArgAllow* blastn_cond =
        new CArgAllow_Doubles(0,100);

    arg_desc->SetConstraint("perc_identity", blastn_cond);

    arg_desc->AddDefaultKey
        ("evalue", "evalue",
         "Expect value (E) for saving hits. Default: 0.0001",
         CArgDescriptions::eDouble, BLAST_EVALUE);

    arg_desc->AddDefaultKey
        ("batch_size", "batch_size",
         "Number of characters of the sequences to be processed " \
         "for every batch of the alignment. Default: 1000000 (1M).",
         CArgDescriptions::eInteger, BLAST_BATCH_SIZE);

    CArgAllow* batch_cond =
        new CArgAllow_Integers(100,1000000000);

    arg_desc->SetConstraint("batch_size", batch_cond);

    arg_desc->AddDefaultKey
        ("num_threads", "num_threads",
         "Number of threads used by BLAST. Default: 1.",
         CArgDescriptions::eInteger, BLAST_THREADS);

    CArgAllow* threads_cond =
        new CArgAllow_Integers(1,32);

    arg_desc->SetConstraint("num_threads", threads_cond);

    arg_desc->AddDefaultKey
        ("word_size", "word_size",
         "BLAST word size. Default: 11.",
         CArgDescriptions::eInteger, BLAST_WORD_SIZE);

    CArgAllow* ws_cond =
        new CArgAllow_Integers(7,30);

    arg_desc->SetConstraint("word_size", ws_cond);

    arg_desc->AddFlag("forcestrandplus",
                      "Force BLAST to align sequences " \
                      "only on strand plus. " \
		      "TRUE if set, FALSE if not set.",
                      true);

    // Setup arg.descriptions for this application
    SetupArgDescriptions(arg_desc.release());
}

int SeqAlignment::Run(void){
    const CArgs& args = GetArgs();
    assert(args.Exist("i"));
    string blast_db;
    if (args["db"].HasValue()) {
      blast_db = args["db"].AsString();
    } else {
      blast_db = args["i"].AsString();
    }

    //Check input files
    CFile in_fasta_file(args["i"].AsString());
    if(!in_fasta_file.Exists()) {
        ERROR("File " << args["i"].AsString() << " not found.");
        return 1;
    }
    CFile in_db_file(blast_db);
    if(!in_db_file.Exists()) {
        ERROR("File " << blast_db << " not found.");
        return 1;
    }
    string outasn = in_fasta_file.GetDir() + in_fasta_file.GetBase() + ".out.asn";
#ifdef PRINT_CSV_ALN
    string outcsv = in_fasta_file.GetDir() + in_fasta_file.GetBase() + ".out.csv";
#endif
    string outmissing = in_fasta_file.GetDir() + in_fasta_file.GetBase() + ".out.missing";

    bool force_strand_plus = false;
    if(args["forcestrandplus"]) {
		force_strand_plus = true;
    }

	int num_threads = 1;
#ifdef ENABLE_OPENMP
    if (args["num_threads"].AsInteger() > 1) {
        if (args["num_threads"].AsInteger() < omp_get_num_procs()) {
            omp_set_num_threads(args["num_threads"].AsInteger());
            num_threads = args["num_threads"].AsInteger();
        } else {
            omp_set_num_threads(omp_get_num_procs());
            num_threads = omp_get_num_procs();
        }
    }
#endif

    // Print input parameters
    INFO("PARAMETERS");
    INFO("Input sequence file: " << args["i"].AsString());
    INFO("Input DB file: " << blast_db);
    INFO("Output ASN file: " << outasn);
#ifdef PRINT_CSV_ALN
    INFO("Output CSV file: " << outcsv);
#endif
    INFO("Output Missing file: " << outmissing);
    INFO("BLAST Word Size: " << args["word_size"].AsString());
    INFO("BLAST Evalue: " << args["evalue"].AsString());
    INFO("BLAST Identity Perc.: " << args["perc_identity"].AsString());
    INFO("BLAST Batch Size: " << args["batch_size"].AsString());
    INFO("BLAST Num. Threads: " << num_threads);
    if(args["forcestrandplus"]) {
		INFO("BLAST forced to align only on strand plus.");
    }

	vector< std::ofstream* > input_streams;
	for(int i=0; i < num_threads; ++i) {
		stringstream ss;
		ss << i;
		string tmp_f_name = args["i"].AsString() + "_" + ss.str();
        std::ofstream* tmp_f_stream = new ofstream(tmp_f_name.c_str());
        if(!tmp_f_stream->is_open()) {
            ERROR("Error in creating " << tmp_f_name << " file.");
            return 1;
        }
		input_streams.push_back(tmp_f_stream);
	}

	gzFile fp = gzopen(args["i"].AsString().c_str(), "r");
	kseq_t* seq = kseq_init(fp);
	int num_seq = 0;
	while ((kseq_read(seq)) >= 0) {
		string id = seq->name.s;
		string s = seq->seq.s;
		int num_thr = num_seq % num_threads;
		(*input_streams[num_thr]) << ">" << id << "\n" << s << "\n";
		num_seq++;
	}
	gzclose(fp);
	for(int i=0; i < num_threads; ++i) {
		input_streams[i]->close();
	}
	input_streams.clear();
	int num_input_seqs = 0;
	int num_aln_seqs = 0;
	int tot_alignments = 0;
	int num_missing_seqs = 0;
	bool blast_error = false;
#pragma omp parallel for schedule(dynamic, 1)
	for(int i=0; i < num_threads; ++i) {
		stringstream ss;
		ss << i;
		BlastExecuter blast(args["i"].AsString() + "_" + ss.str(),
				    blast_db,
				    outasn + "_" + ss.str(),
#ifdef PRINT_CSV_ALN
				    outcsv + "_" + ss.str(),
#endif
				    outmissing + "_" + ss.str(),
				    args["word_size"].AsInteger(),
				    args["evalue"].AsDouble(),
				    args["perc_identity"].AsDouble(),
				    args["batch_size"].AsInteger(),
				    1,
				    force_strand_plus);
		int blast_stat = blast.executeBlast();
#pragma omp critical
	{
		if(blast_stat == 1) {
			ERROR("Problem in executing BLAST.");
			blast_error = true;
		}
		num_input_seqs += blast.num_input_seqs;
		num_aln_seqs += blast.num_aln_seqs;
		tot_alignments += blast.tot_alignments;
		num_missing_seqs += blast.num_missing_seqs;
	}

	}
#ifdef PRINT_CSV_ALN
	//CSV
	std::ofstream out_csv_stream(outcsv.c_str(), std::ios_base::binary);
	if(!out_csv_stream.is_open()) {
		ERROR("Error in creating " << outcsv << " file.");
		return 1;
	}
#endif
	//ASN
	std::ofstream out_asn_stream(outasn.c_str(), std::ios_base::binary);
	if(!out_asn_stream.is_open()) {
		ERROR("Error in creating " << outasn << " file.");
		return 1;
	}
	//MISSING
	std::ofstream out_missing_stream(outmissing.c_str(), std::ios_base::binary);
	if(!out_missing_stream.is_open()) {
		ERROR("Error in creating " << outmissing << " file.");
		return 1;
	}
	for(int i=0; i < num_threads; ++i) {
		stringstream ss;
		ss << i;
		//FASTA
		string res_fasta_name = args["i"].AsString() + "_" + ss.str();
		remove(res_fasta_name.c_str());
#ifdef PRINT_CSV_ALN
		//CSV
		string res_csv_name = outcsv + "_" + ss.str();
		CFile res_csv_file(res_csv_name);
		if(res_csv_file.GetLength() > 0) {
		    std::ifstream res_csv_stream(res_csv_name.c_str(), std::ios_base::binary);
		    if(!res_csv_stream.is_open()) {
			ERROR("Error in opening " << res_csv_name.c_str() << " file.");
			return 1;
		    }
		    out_csv_stream << res_csv_stream.rdbuf();
		    res_csv_stream.close();
		}
		res_csv_file.Remove();
#endif
		//ASN
		string res_asn_name = outasn + "_" + ss.str();
		CFile res_asn_file(res_asn_name);
		if(res_asn_file.GetLength() > 0) {
		    std::ifstream res_asn_stream(res_asn_name.c_str(), std::ios_base::binary);
		    if(!res_asn_stream.is_open()) {
			ERROR("Error in opening " << res_asn_name.c_str() << " file.");
			return 1;
		    }
		    out_asn_stream << res_asn_stream.rdbuf();
		    res_asn_stream.close();
		}
		res_asn_file.Remove();
		//MISSING
		string res_missing_name = outmissing + "_" + ss.str();
		CFile res_missing_file(res_missing_name);
		if(res_missing_file.GetLength() > 0) {
		    std::ifstream res_missing_stream(res_missing_name.c_str(), std::ios_base::binary);
		    if(!res_missing_stream.is_open()) {
			ERROR("Error in opening " << res_missing_name.c_str() << " file.");
			return 1;
		    }
		    out_missing_stream << res_missing_stream.rdbuf();
		    res_missing_stream.close();
		}
		res_missing_file.Remove();
	}
#ifdef PRINT_CSV_ALN
	out_csv_stream.flush();
	out_csv_stream.close();
#endif
	out_asn_stream.flush();
	out_asn_stream.close();
	out_missing_stream.flush();
	out_missing_stream.close();

	if(blast_error) {
		ERROR("BLAST execution error.");
		return 1;
	}

    //Print Results
    INFO("BLAST RESULTS");
    INFO("Num. Input Sequences: " << num_input_seqs);
    INFO("Num. Aligned Sequences: " << num_aln_seqs);
    INFO("Num. Total Alignments: " << tot_alignments);
    INFO("Num. Not Aligned Sequences: " << num_missing_seqs);
    return 0;
}

int main(int argc, char* argv[]){
    INFO("Program started");
    SeqAlignment app;
    return app.AppMain(argc, argv);
}
