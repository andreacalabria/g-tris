/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#include "alignments.hpp"

BEGIN_CLASS_INFO(AlnData) {
    ADD_STD_MEMBER(target_id);
    ADD_STD_MEMBER(query_start);
    ADD_STD_MEMBER(query_stop);
    ADD_STD_MEMBER(target_start);
    ADD_STD_MEMBER(target_stop);
    ADD_STD_MEMBER(query_strand);
    ADD_STD_MEMBER(target_strand);
    ADD_STD_MEMBER(alignment_len);
    ADD_STD_MEMBER(score);
}
END_CLASS_INFO;

BEGIN_CLASS_INFO(Alignments) {
    ADD_MEMBER(query_id, STD, (string));
    ADD_MEMBER(query_len, STD, (int));
    ADD_MEMBER(masked, STL_list, (STD, (string)));
    ADD_MEMBER(max_score, STD, (int));
    ADD_MEMBER(al_data, STL_list, (CLASS, (AlnData)));
}
END_CLASS_INFO;

#ifdef PRINT_CSV_ALN
void printAln(const Alignments& a, ostream& os) {
    for (std::list< AlnData >::const_iterator it = a.al_data.begin();
        it != a.al_data.end(); ++it) {
        //Fasta ID of first sequence (query)
        os << a.query_id << ",";
        //Fasta ID of second sequence (target)
        os << it->target_id << ",";
        //Start position of first sequence
        os << it->query_start << ",";
        //Stop position of first sequence
        os << it->query_stop << ",";
        //Stand of first sequence (1 or 2)
        os << it->query_strand << ",";
        //Start position of second sequence
        os << it->target_start << ",";
        //Stop position of second sequence
        os << it->target_stop << ",";
        //Strand of second sequence (1 or 2)
        os << it->target_strand << ",";
        //Alignment length (gaps included)
        os << it->alignment_len << ",";
        //Length of the query sequence
        os << a.query_len << ",";
        //Alignment score
        os << it->score;
        os << "\n";
    }
}
#endif
