/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#include "log.hpp"
#include "alignments.hpp"
#include "configuration.hpp"
#include "iscluster.hpp"
#include "uniqueseqs.hpp"
#include "consensus.hpp"

#include <queue>
#include <iterator>

#include <serial/serial.hpp>
#include <serial/objostr.hpp>
#include <serial/objistr.hpp>

//Kseq fasta parser
#include <zlib.h>
#include <stdio.h>
#include "kseq.h"

USING_NCBI_SCOPE;
USING_SCOPE(objects);

class CreateSimpleClusters : public CNcbiApplication {
    virtual void Init(void);
    virtual int Run(void);
    std::list< ISCluster > initialClusters(const std::map< string, Alignments >& seq_aln,
					   ofstream& out_bed_stream,
					   std::map< string,
					   std::list< string > >& fasta_id_map,
					   const string& subg_dir_name);
};

void CreateSimpleClusters::Init(void) {
auto_ptr<CArgDescriptions> arg_desc(new CArgDescriptions);
    // Specify USAGE context
    CNcbiApplication::DisableArgDescriptions();

    arg_desc->SetUsageContext(GetArguments().GetProgramBasename(),
                              "Program to create clusters from not mapped consensus sequences.");

    arg_desc->AddKey
        ("i", "inputfile",
         "Input file in ASN format containing "                         \
         "the alignments of iterativealignment program.",               \
         CArgDescriptions::eInputFile, CArgDescriptions::fPreOpen);

    arg_desc->AddKey
        ("f", "consFastaFile",
         "Input file in Fasta format containing "			\
         "the consensus sequences.",					\
         CArgDescriptions::eInputFile, CArgDescriptions::fPreOpen);

    arg_desc->AddOptionalKey
        ("m", "mapfile",
         "File containing the map of unique sequences " \
         "obtained with option '-uniqueseqs' of "       \
         "makeblastdb program.",
         CArgDescriptions::eInputFile);
    
    arg_desc->AddDefaultKey
        ("num_threads", "num_threads",
         "Number of threads used by BLAST. Default: 1.",
         CArgDescriptions::eInteger, BLAST_THREADS);

    arg_desc->AddFlag("no-consensus",
                      "Disable final consensus computation. "	\
		      "No computation is set.",			\
		      true);

    // Setup arg.descriptions for this application
    SetupArgDescriptions(arg_desc.release());
}

std::list< ISCluster >
    CreateSimpleClusters::initialClusters(const std::map< string, Alignments >& seq_aln,
					  ofstream& out_bed_stream,
					  std::map< string,
					  std::list< string > >& fasta_id_map,
					  const string& subg_dir_name) {

    std::list< ISCluster > clu;
    std::map< string, Alignments >::const_iterator seq_aln_it;
    for(seq_aln_it = seq_aln.begin(); seq_aln_it != seq_aln.end(); seq_aln_it++) {
	int weight = 0;
        try{
	    string inmissubg = subg_dir_name + "/" + seq_aln_it->first + ".fa";
            gzFile fp = gzopen(inmissubg.c_str(), "r");
            kseq_t* seq = kseq_init(fp);
            while ((kseq_read(seq)) >= 0) {
                string id = seq->name.s;
                weight += fasta_id_map.at(id).size() + 1;
            }
            gzclose(fp);
        } catch(exception& e) {
            ERROR("Mis. subgraph file error: " << e.what());
        }
	Alignments al = seq_aln_it->second;
	for (std::list< AlnData >::iterator it = al.al_data.begin();
	     it != al.al_data.end(); ++it) {
	    std::list< AlnData >::iterator un_it = it;
	    un_it++;
	    for(;un_it != al.al_data.end();) {
		if(un_it->target_id == it->target_id &&
		   un_it->query_strand == it->query_strand &&
		   un_it->target_strand == it->target_strand) {
		    if (it->target_strand == it->query_strand) {
			if(un_it->target_start == it->target_start) {
			    if(un_it->score > it->score) {
				it->score = un_it->score;
			    }
			    un_it = al.al_data.erase(un_it);
			} else {
			    un_it++;
			}
		    } else {
			if(un_it->target_stop == it->target_stop) {
			    if(un_it->score > it->score) {
				it->score = un_it->score;
			    }
			    un_it = al.al_data.erase(un_it);
			} else {
			    un_it++;
			}
		    }
		} else {
		    un_it++;
		}
	    }
        }
	clu.push_back(ISCluster(seq_aln_it->first, weight, al));
    }

    //Print clusters in BED format
    out_bed_stream << "track name=clusters ";
    out_bed_stream << "description=\"Integration Sites Clusters.\" ";
    out_bed_stream << "useScore=1" << std::endl;
    std::list< ISCluster >::const_iterator clu_it;
    for (clu_it = clu.begin(); clu_it != clu.end(); ++clu_it) {
	out_bed_stream << *clu_it;
    }
    INFO("Num. Missing Initial Clusters: " << clu.size());
    return clu;
}

int CreateSimpleClusters::Run(void){
    const CArgs& args = GetArgs();
    assert(args.Exist("i"));
    //Chech input files
    CFile in_asn_file(args["i"].AsString());
    if(!in_asn_file.Exists()) {
	ERROR("File " << args["i"].AsString() << " not found.");
	return 1;
    }
    if(args["m"].HasValue()) {
	CFile in_map_file(args["m"].AsString());
	if(!in_map_file.Exists()) {
	    ERROR("File " << args["m"].AsString() << " not found.");
	    return 1;
	}
    }
    int num_threads = 1;
    num_threads = args["num_threads"].AsInteger();
    
    bool consensus = true;
    if(args["no-consensus"]) {
	INFO("Avoid to compute final consensus sequences.");
	consensus = false;
    }
    //Subggraph directory
    string subg_dir_name(in_asn_file.GetDir() + "subgraphs");
    CDir subg_dir(subg_dir_name);
    if(!subg_dir.Exists()) {
        ERROR("Cannot find subgraph directory: " << subg_dir_name << ".");
	return 1;
    }
    //Bed output
    string outbed = in_asn_file.GetDir() + in_asn_file.GetBase() + ".bed";
    ofstream out_bed_stream(outbed.c_str());
    if(!out_bed_stream.is_open()) {
	ERROR("Error in creating " << outbed << " file.");
	return 1;
    }
    //Dot output
    string outdot = in_asn_file.GetDir() + in_asn_file.GetBase() + ".dot";
    ofstream out_dot_stream(outdot.c_str());
    if(!out_dot_stream.is_open()) {
	ERROR("Error in creating " << outdot << " file.");
	return 1;
    }
    //Cluster (serialized) output
    string outclu = in_asn_file.GetDir() + in_asn_file.GetBase() + ".clu";
    auto_ptr<CObjectOStream>
        out_clu_stream(CObjectOStream::Open(CLU_TYPE, outclu));
    if(!out_clu_stream->InGoodState()) {
	ERROR("Error in creating " << outclu << " file.");
	return 1;
    }

    INFO("PARAMETERS");
    INFO("Input ASN file: " << args["i"].AsString());
    if(args["m"].HasValue()) {
        INFO("Input Map file: " << args["m"].AsString());
    }
    INFO("Input subgraph dir.: " << subg_dir_name);
    INFO("Output BED file: " << outbed);
    INFO("Output CLU file: " << outclu);

    INFO("Started reading input files.");
    auto_ptr< CObjectIStream > in_asn_stream
        (CObjectIStream::Open(ASN_TYPE, args["i"].AsString()));
    if(!in_asn_stream->InGoodState()) {
	ERROR("Error in opening " << args["i"].AsString() << " file.");
	return 1;
    }

    //Read genomic alignments
    std::map< string, Alignments > seq_aln;
    while(!in_asn_stream->EndOfData()) {
        Alignments al;
        *in_asn_stream >> al;
        seq_aln[al.query_id] = al;
    }

    //Read consensus sequences
    std::map< string, string > cons_map;
    try {
	gzFile fp = gzopen(args["f"].AsString().c_str(), "r");
	kseq_t* seq = kseq_init(fp);
	while((kseq_read(seq)) >= 0) {
	    cons_map[seq->name.s] = seq->seq.s;
	}
	gzclose(fp);
    } catch(exception& e) {
	ERROR("Error in reading consensus sequences: " << e.what());
	return 1;
    }

    //Read serialized FASTA Id Map
    UniqueSeqs unique_seq_map;
    if(args["m"].HasValue()) {
        auto_ptr< CObjectIStream > in_map_stream
            (CObjectIStream::Open(ASN_TYPE, args["m"].AsString()));
        *in_map_stream >> unique_seq_map;
    }

    INFO("Finished reading input files.");

    //Create simple initial clusters
    INFO("Started creating missing initial clusters.");
    std::list< ISCluster > clu = initialClusters(seq_aln,
						 out_bed_stream,
						 unique_seq_map.fasta_id_map,
						 subg_dir_name);
    INFO("Finished creating missing initial clusters.");

    //Merge simple clusters
    INFO("Started merging initial clusters.");
    int num_merge = refineClusters(clu, out_dot_stream);
    INFO("Finished merging initial clusters.");

    if(consensus) {
	//Compute consensus sequences for clusters
	INFO("Start computing new consensus sequences.");
	if(computeClusterConsensus(clu,
				   unique_seq_map.fasta_id_map,
				   cons_map,
				   subg_dir_name,
				   num_threads) == 1) {
	    ERROR("Consensus error.");
	    return 1;
	}
	INFO("Finished computing new consensus sequences.");
    }
    INFO("Num. Final Clusters: " << clu.size());
    INFO("Num. Merged Clusters: " << num_merge);

    //Serialize the output
    std::list< ISCluster >::iterator clu_it;
    for(clu_it = clu.begin(); clu_it != clu.end(); ++clu_it) {
        *out_clu_stream << *clu_it;
    }
    //Flush and Close
    out_bed_stream.flush();
    out_bed_stream.close();
    out_dot_stream.flush();
    out_dot_stream.close();
    out_clu_stream->Flush();
    return 0;
}

int main(int argc, char* argv[]){
    INFO("Program started");
    CreateSimpleClusters app;
    return app.AppMain(argc, argv);
}
