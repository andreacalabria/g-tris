/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#include "log.hpp"
#include "alignments.hpp"
#include "configuration.hpp"
#include "iscluster.hpp"

#include <queue>
#include <iterator>
#include <sstream>

#include <serial/serial.hpp>
#include <serial/objostr.hpp>
#include <serial/objistr.hpp>

#include <Snap.h>

USING_NCBI_SCOPE;
USING_SCOPE(objects);

class ComputeMatrix : public CNcbiApplication {
    virtual void Init(void);
    virtual int Run(void);
    int readGraph(string& graph_file,
		  std::map< string, std::pair< int, string > >& nodes,
		  std::set< std::pair< string, string > >& edges);
};

void readSNAPGraph(PUNGraph& g,
		   std::map< int, std::string >& cluster_id,
		   std::map< int, int >& weight,
		   std::map< int, std::string >& exp_label,
		   ifstream& in) {
    string str = "";
    do {
	std::getline (in, str);
    }while(str != "@nodes");
    // Read col names
    std::getline(in, str);
    string prev = "";
    do {
    	std::getline (in, str);
    	if(prev != "") {
	    std::istringstream s(prev);
	    int nodeId;
	    std::vector< std::string > res((std::istream_iterator< std::string > (s)),
					   std::istream_iterator< std::string >());
	    nodeId = atoi(res[0].c_str());
	    cluster_id[nodeId] = res[1];
	    weight[nodeId] = atoi(res[2].c_str());
	    exp_label[nodeId] = res[3];
	    g->AddNode(nodeId);
    	}
    	prev = str;    
    }while(str != "@edges");
    // Read col names
    std::getline(in, str);
    while(std::getline (in, str)) {
	istringstream s(str);
	int srcNodeId;
	s >> srcNodeId;
	int destNodeId;
	s >> destNodeId;
	if (!g->IsNode(srcNodeId)) {
	    ERROR("Error: missing node of " << srcNodeId);
	    return;
	}
	if (!g->IsNode(destNodeId)) {
	    ERROR("Error: missing node of " << destNodeId);
	    return;
	}
	g->AddEdge(srcNodeId, destNodeId);
    }
}

void ComputeMatrix::Init(void) {
    auto_ptr<CArgDescriptions> arg_desc(new CArgDescriptions);
    // Specify USAGE context
    CNcbiApplication::DisableArgDescriptions();

    arg_desc->SetUsageContext(GetArguments().GetProgramBasename(),
                              "Program to compute the matrix based on cluster " \
			      "similarities obtained by findclusters procedure.");

    arg_desc->AddKey
        ("gf", "graphs_file",
         "Input file containing a list of graph similarities "	     \
         "each one obtained by findclusters procedure.",					\
         CArgDescriptions::eInputFile, CArgDescriptions::fPreOpen);

     arg_desc->AddKey
	("o", "out",
	 "Output matrix file in CSV format.",
	 CArgDescriptions::eOutputFile, CArgDescriptions::fPreOpen);

    // Setup arg.descriptions for this application
    SetupArgDescriptions(arg_desc.release());
}

int ComputeMatrix::readGraph(string& graph_file,
			     std::map< string, std::pair< int, string > >& nodes,
			     std::set< std::pair< string, string > >& edges) {
    ifstream graph_stream(graph_file.c_str());
    if(!graph_stream.is_open()) {
	ERROR("Error in opening " << graph_file << " file.");
	return 1;
    }
    PUNGraph g = TUNGraph::New();
    std::map< int, std::string > cluster_id;
    std::map< int, int > weight;
    std::map< int, std::string > exp_label;

    readSNAPGraph(g, cluster_id, weight, exp_label, graph_stream);

    for (TUNGraph::TNodeI ni = g->BegNI(); ni < g->EndNI(); ni++) {
	int node_id = ni.GetId();
	if(nodes.find(cluster_id[node_id]) == nodes.end()) {
	    nodes[cluster_id[node_id]] = make_pair(weight[node_id],
						   exp_label[node_id]);
	} else {
	    if(nodes[cluster_id[node_id]].first != weight[node_id] ||
	       nodes[cluster_id[node_id]].second != exp_label[node_id]) {
		ERROR("Error: cluster information mismatch.");
		return 1;
	    }
	}
    }
    for (TUNGraph::TEdgeI ei = g->BegEI(); ei < g->EndEI(); ei++) {
	edges.insert(std::make_pair(cluster_id[ei.GetSrcNId()],
				    cluster_id[ei.GetDstNId()]));
    }
    INFO("Graph: " << graph_file);
    INFO("Num. Nodes: " << g->GetNodes());
    INFO("Num. Edges: " << g->GetEdges());

    return 0;
}

int ComputeMatrix::Run(void){
    const CArgs& args = GetArgs();
    assert(args.Exist("gf"));

    //Check input files
    CFile in_graphfile_file(args["gf"].AsString());
    if(!in_graphfile_file.Exists()) {
	ERROR("File " << args["gf"].AsString() << " not found.");
	return 1;
    }

    CFile out_csv_file(args["o"].AsString());
    //Output CSV matrix
    ofstream out_csv_stream(args["o"].AsString().c_str());
    if(!out_csv_stream.is_open()) {
	ERROR("Error in creating " << args["o"].AsString() << " file.");
	return 1;
    }
    //Output DOT graph
    string out_dot_file = out_csv_file.GetDir() + out_csv_file.GetBase() + ".dot";
    ofstream out_dot_stream(out_dot_file.c_str());
    if(!out_dot_stream.is_open()) {
	ERROR("Error in creating " << out_dot_file << " file.");
	return 1;
    }
    //Output cluster_id association
    string out_cluid_file = out_csv_file.GetDir() + out_csv_file.GetBase() + "_clu_id.csv";
    ofstream out_cluid_stream(out_cluid_file.c_str());
    if(!out_cluid_stream.is_open()) {
	ERROR("Error in creating " << out_cluid_file << " file.");
	return 1;
    }
    //Output representative clusters
    string out_repr_clu_file = out_csv_file.GetDir() + out_csv_file.GetBase() + "_repr_clu.csv";
    ofstream out_repr_clu_stream(out_repr_clu_file.c_str());
    if(!out_repr_clu_stream.is_open()) {
	ERROR("Error in creating " << out_repr_clu_file << " file.");
	return 1;
    }


    INFO("PARAMETERS");
    INFO("Input file of graphs: " << args["gf"].AsString());
    INFO("Output CSV file: " << args["o"].AsString());
    INFO("Outout DOT file: " << out_dot_file);
    INFO("Output Cluster_ID Assoc. file: " << out_cluid_file);
    INFO("Outout Representative Clusters file: " << out_repr_clu_file);

    INFO("Started reading input files.");
    std::map< string, std::pair< int, string > > nodes;
    std::set< std::pair< string, string > > edges;

    std::ifstream in_graphfile_stream(args["gf"].AsString().c_str());
    if(!in_graphfile_stream.is_open()) {
	ERROR("Error in opening " << args["gf"].AsString() << " file.");
	return 1;
    }
    string line;
    int num_graphs = 0;
    while(std::getline(in_graphfile_stream, line)) {
	if(readGraph(line, nodes, edges) == 0) {
	    num_graphs++;
	}
    }
    in_graphfile_stream.close();

    INFO("Finished reading input files.");
    INFO("Number of graph read: " << num_graphs);

    INFO("Started creating final graph.");
    //Graph structure
    PUNGraph g = TUNGraph::New();
    std::map< int, std::string > nodes_id;
    std::map< std::string, int > inv_nodes;
    //Exp. names map
    std::map< string, int > exp_names;

    //DOT file
    out_dot_stream << "graph {\n";
    out_dot_stream << "rankdir=LR;\n";

    std::map< string, std::pair< int, string > >::const_iterator n_it;
    int node_id = 0;
    for(n_it = nodes.begin(); n_it != nodes.end(); ++n_it) {
	g->AddNode(node_id);
     	nodes_id[node_id] = n_it->first;
     	inv_nodes[n_it->first] = node_id;
     	exp_names[n_it->second.second] = 0;
     	out_dot_stream << n_it->first << " [label=\"\", shape=ellipse, width=0.1, height=0.1];\n";
	node_id++;
    }

    std::set< std::pair< string, string > >::const_iterator e_it;
    for(e_it = edges.begin(); e_it != edges.end(); ++e_it) {
    	g->AddEdge(inv_nodes[e_it->first],
		   inv_nodes[e_it->second]);
    	out_dot_stream << e_it->first << " -- "
    		       << e_it->second << ";"
    		       << "\n";
    }
    INFO("Finished creating final graph.");
    INFO("Tot. Num. Nodes: " << g->GetNodes());
    INFO("Tot. Num. Edges: " << g->GetEdges());

    INFO("Started computing matrix.");
    TCnComV conComV;
    TSnap::GetWccs(g, conComV);
    int num_cc = conComV.Len();
    
    //Cluster_id matrix
    std::vector< std::list< std::string > > clu_id_matr(num_cc);
    //Matrix
    std::vector< std::map< std::string, int > > matrix(num_cc, exp_names);
    for(int cc = 0; cc < num_cc; cc++) {
	const TIntV& nodeIdV = conComV[cc].NIdV;
	for(int nid = 0; nid < nodeIdV.Len(); nid++) {
	    int node_id = nodeIdV[nid].Val;
	    string exp = nodes[nodes_id[node_id]].second;
	    int weight = nodes[nodes_id[node_id]].first;
	    matrix[cc][exp] += weight;
	    clu_id_matr[cc].push_back(nodes_id[node_id]);
	}
    }

    int totw = 0;
    //Print CSV header (first line)
    out_csv_stream << "Cluster" << FS_CSV;
    for(std::map< string, int >::const_iterator r_it = exp_names.begin();
    	r_it != exp_names.end(); ++r_it) {
     	out_csv_stream << r_it->first << FS_CSV;
    }
    out_csv_stream << "\n";

    //Print CSV data
    for(unsigned int m_r = 0; m_r < matrix.size(); ++m_r) {
    	out_csv_stream << m_r << FS_CSV;
    	std::map< string, int >::const_iterator r_it;
    	for(r_it = matrix.at(m_r).begin(); r_it != matrix.at(m_r).end(); ++r_it) {
    	    out_csv_stream << r_it->second << FS_CSV;
    	    totw += r_it->second;
    	}
    	out_csv_stream << "\n";
    }
    out_csv_stream.flush();
    out_csv_stream.close();

    out_dot_stream << "}\n";
    out_dot_stream.flush();
    out_dot_stream.close();

    //Print cluster_id matrix
    for(unsigned int cl_m = 0; cl_m < clu_id_matr.size(); ++cl_m) {
	out_cluid_stream << cl_m << FS_CSV;
     	std::list< string >::const_iterator l_it;
     	int max_out_deg = -1;
     	string max_clu = "";
	for(l_it = clu_id_matr.at(cl_m).begin(); l_it != clu_id_matr.at(cl_m).end(); ++l_it) {
	    out_cluid_stream << *l_it << FS_CSV;
     	    //Select cluster with max out degree
     	    int out_deg = 0;
	    TUNGraph::TNodeI NI = g->GetNI(inv_nodes[*l_it]);
	    for(int e = 0; e < NI.GetOutDeg(); e++) {
		out_deg++;
	    }
    	    if(out_deg > max_out_deg) {
    		max_out_deg = out_deg;
    		max_clu = *l_it;
    	    }
    	}
    	out_repr_clu_stream << cl_m << FS_CSV << max_clu << "\n";
        out_cluid_stream << "\n";
    }
    out_cluid_stream.flush();
    out_cluid_stream.close();
    out_repr_clu_stream.flush();
    out_repr_clu_stream.close();

    INFO("Finished computing matrix.");
    INFO("Num. Matrix Final Clusters: " << num_cc);
    INFO("Tot Num. Reads: " << totw);
    return 0;
}

int main(int argc, char* argv[]){
    INFO("Program started");
    ComputeMatrix app;
    return app.AppMain(argc, argv);
}
