/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#include <ncbi_pch.hpp>
#include "masked_range_set.hpp"

#ifndef SKIP_DOXYGEN_PROCESSING
USING_NCBI_SCOPE;
USING_SCOPE(objects);
#endif /* SKIP_DOXYGEN_PROCESSING */

CMaskedRangesVector &
CMaskedRangeSet::GetRanges(const list< CRef<CSeq_id> > & idlist)
{
    // For each algorithm for which data is provided
    
    NON_CONST_ITERATE(CMaskedRangesVector, algo_iter, m_Ranges) {
        algo_iter->offsets.resize(0);
        int algo_id = algo_iter->algorithm_id;
        
        // Combine Seq-loc ranges for all provided Seq-ids.
        
        CConstRef<CSeq_loc> oid_ranges;
        
        ITERATE(list< CRef<CSeq_id> >, id_iter, idlist) {
            CSeq_id_Handle idh = CSeq_id_Handle::GetHandle(**id_iter);
            x_FindAndCombine(oid_ranges, algo_id, idh);
        }
        
        if (oid_ranges.Empty()) {
            continue;
        }
        
        for(CSeq_loc_CI ranges(*oid_ranges); ranges; ++ranges) {
            CSeq_loc::TRange rng = ranges.GetRange();
            pair<TSeqPos, TSeqPos> pr(rng.GetFrom(), rng.GetToOpen());
            
            algo_iter->offsets.push_back(pr);
        }
    }
    
    return m_Ranges;
}

void CMaskedRangeSet::x_FindAndCombine(CConstRef<CSeq_loc> & L1,
                                       int                   algo_id,
                                       CSeq_id_Handle      & idh)
{
    if ((int)m_Values.size() > algo_id) {
        const TAlgoMap & m = m_Values[algo_id];
        
        TAlgoMap::const_iterator iter = m.find(idh);
        
        if (iter != m.end()) {
            x_CombineLocs(L1, *iter->second);
        }
    }
}

void CMaskedRangeSet::x_CombineLocs(CConstRef<CSeq_loc> & L1,
                                    const CSeq_loc      & L2)
{
    if (L1.Empty()) {
        L1.Reset(& L2);
    } else {
        L1 = L1->Add(L2, CSeq_loc::fMerge_All | CSeq_loc::fSort, NULL);
    }
}

CConstRef<CSeq_loc> & CMaskedRangeSet::x_Set(int algo_id, CSeq_id_Handle idh)
{
    if ((int)m_Values.size() <= algo_id) {
        m_Values.resize(algo_id+1);
    }
    if (m_Values[algo_id].empty()) {
        m_Ranges.resize(m_Ranges.size()+1);
        m_Ranges[m_Ranges.size()-1].algorithm_id = algo_id;
    }
    return m_Values[algo_id][idh];
}

