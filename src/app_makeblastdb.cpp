/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

/** @file makeblastdb.cpp
 * Command line tool to create BLAST databases. This is the successor to
 * formatdb from the C toolkit
 */

#include "configuration.hpp"

#include <algo/blast/blastinput/blast_input.hpp>
#include <serial/iterator.hpp>
#include <objmgr/util/sequence.hpp>

#include <map>
#include <list>
#include <sstream>
#include <serial/objistr.hpp>
#include <serial/objostr.hpp>
#include <serial/serialimpl.hpp>
#include <serial/serial.hpp>

#include "blast_app_util.hpp"
#include "masked_range_set.hpp"

#include <zlib.h>
#include <stdio.h>
#include "kseq.h"

#include "uniqueseqs.hpp"

KSEQ_INIT(gzFile, gzread)

USING_NCBI_SCOPE;
USING_SCOPE(blast);
USING_SCOPE(objects);

/// The main application class
class CMakeBlastDBApp : public CNcbiApplication {
public:
    /// Convenience typedef
    typedef CFormatGuess::EFormat TFormat;

    /** @inheritDoc */
    CMakeBlastDBApp() : m_LogFile(NULL) {
        CRef<CVersion> version(new CVersion());
        version->SetVersionInfo(new CBlastVersion());
        SetFullVersion(version);
    }

private:
    /** @inheritDoc */
    virtual void Init();
    /** @inheritDoc */
    virtual int Run();

    string compute_unique_seqs(const string& in_file);
    TFormat x_GuessFileType(CNcbiIstream & input);
    void x_BuildDatabase(string in_file);
    void x_AddFasta(CNcbiIstream & data);
    void x_ProcessInputData(const string & paths);
    void x_VerifyInputFilesType(const string& seq_file);

    // Data
    CNcbiOstream * m_LogFile;
    CRef<CBuildDatabase> m_DB;
    CRef<CMaskedRangeSet> m_Ranges;
};

/// Reads an object defined in a NCBI ASN.1 spec from a stream in multiple
/// formats: binary and text ASN.1 and XML
/// @param file stream to read the object from [in]
/// @param fmt specifies the format in which the object is encoded [in]
/// @param obj on input is an empty CRef<> object, on output it's populated
/// with the object read [in|out]
/// @param msg error message to display if reading fails [in]
template<class TObj>
void s_ReadObject(CNcbiIstream          & file,
                  CFormatGuess::EFormat   fmt,
                  CRef<TObj>            & obj,
                  const string          & msg)
{
    obj.Reset(new TObj);

    switch (fmt) {
    case CFormatGuess::eBinaryASN:
        file >> MSerial_AsnBinary >> *obj;
        break;

    case CFormatGuess::eTextASN:
        file >> MSerial_AsnText >> *obj;
        break;

    default:
        NCBI_THROW(CInvalidDataException, eInvalidInput, string("Unknown encoding for ") + msg);
    }
}

/// Overloaded version of s_ReadObject which uses CFormatGuess to determine
/// the encoding of the object in the file
/// @param file stream to read the object from [in]
/// @param obj on input is an empty CRef<> object, on output it's populated
/// with the object read [in|out]
/// @param msg error message to display if reading fails [in]
template<class TObj>
void s_ReadObject(CNcbiIstream & file,
                  CRef<TObj>    & obj,
                  const string  & msg)
{
    CFormatGuess fg(file);
    fg.GetFormatHints().AddPreferredFormat(CFormatGuess::eBinaryASN);
    fg.GetFormatHints().AddPreferredFormat(CFormatGuess::eTextASN);
    fg.GetFormatHints().DisableAllNonpreferred();
    s_ReadObject(file, fg.GuessFormat(), obj, msg);
}

/// Command line flag to represent the input
static const string kInput("i");
/// Defines token separators when multiple inputs are present
static const string kInputSeparators(" ");
/// Command line flag to represent the output
static const string kOutput("out");

void CMakeBlastDBApp::Init() {
    HideStdArgs(fHideConffile | fHideFullVersion | fHideXmlHelp | fHideDryRun);

    auto_ptr<CArgDescriptions> arg_desc(new CArgDescriptions);

    // Specify USAGE context
    arg_desc->SetUsageContext(GetArguments().GetProgramBasename(),
                              "Application to create a BLAST database " \
                              "from NUCLEOTIDE Fasta sequences.");

    arg_desc->SetCurrentGroup("Input options");
    arg_desc->AddKey(kInput, "input_file",
                            "Input file/database name",
                            CArgDescriptions::eInputFile);

    arg_desc->AddDefaultKey("max_file_sz", "number_of_bytes",
                            "Maximum file size for BLAST database files",
                            CArgDescriptions::eString, "1GB");

    arg_desc->AddFlag("uniqueseqs",
                      "From the input file fasta, " \
                      "compute only on unique sequences " \
                      "(unique by sequence and not by id). " \
                      "TRUE if set, FALSE if not set.",
                      true);

    SetupArgDescriptions(arg_desc.release());
}

/// Converts a Uint8 into a string which contains a data size (converse to
/// NStr::StringToUInt8_DataSize)
/// @param v value to convert [in]
/// @param minprec minimum precision [in]
static string Uint8ToString_DataSize(Uint8 v, unsigned minprec = 10) {
    static string kMods = "KMGTPEZY";
    size_t i(0);
    for(i = 0; i < kMods.size(); i++) {
        if (v < Uint8(minprec)*1024) {
            v /= 1024;
        }
    }

    string rv = NStr::UInt8ToString(v);
    if (i) {
        rv.append(kMods, i, 1);
        rv.append("B");
    }

    return rv;
}

void CMakeBlastDBApp::x_AddFasta(CNcbiIstream & data) {
    m_DB->AddFasta(data);
}

static bool s_GenerateTitle(const CBioseq & bio) {
    if (! bio.CanGetDescr()) {
        return true;
    }

    ITERATE(list< CRef< CSeqdesc > >, iter, bio.GetDescr().Get()) {
        const CSeqdesc & desc = **iter;
        if (desc.IsTitle()) {
            return false;
        }
    }

    return true;
}

class CSeqEntrySource : public IBioseqSource {
public:
    /// Convenience typedef
    typedef CFormatGuess::EFormat TFormat;
    CSeqEntrySource(CNcbiIstream & is, TFormat fmt)
        :m_objmgr(CObjectManager::GetInstance()),
         m_scope(new CScope(*m_objmgr)),
         m_entry(new CSeq_entry)
    {
        char ch=is.peek();

        // Get rid of white spaces
        while(!is.eof() && (ch==' ' || ch=='\t' || ch=='\n' || ch=='\r')) {
            is.read(&ch, 1);
            ch=is.peek();
        }
        if(is.eof())
            return;

        // If input is a Bioseq_set
        if(ch == 'B' || ch == '0') {
            CRef<CBioseq_set> obj(new CBioseq_set);
            s_ReadObject(is, fmt, obj, "bioseq");
            m_entry->SetSet(*obj);
        } else {
            s_ReadObject(is, fmt, m_entry, "bioseq");
        }

        m_bio = Begin(*m_entry);
        for (CTypeIterator<CBioseq> it = Begin(*m_entry); it; ++it) {
            m_scope->AddBioseq(*it);
        }
        m_entry->Parentize();
    }

    virtual CConstRef<CBioseq> GetNext() {
        CConstRef<CBioseq> rv;

        if (m_bio ) {
            if (s_GenerateTitle(*m_bio)) {
                sequence::CDeflineGenerator gen;
                const string & title = gen.GenerateDefline(*m_bio , *m_scope);
                CRef<CSeqdesc> des(new CSeqdesc);
                des->SetTitle(title);
                CSeq_descr& desr(m_bio ->SetDescr());
                desr.Set().push_back(des);
            }
            rv.Reset(&(*m_bio ));
            ++m_bio ;
        }

        return rv;
    }

private:
    CRef<CObjectManager>    m_objmgr;
    CRef<CScope>            m_scope;
    CRef<CSeq_entry>        m_entry;
    CTypeIterator <CBioseq> m_bio;
};

class CRawSeqDBSource : public IRawSequenceSource {
public:
    CRawSeqDBSource(const string & name, CBuildDatabase * outdb);

    virtual ~CRawSeqDBSource() {
        if (m_Sequence) {
            m_Source->RetSequence(& m_Sequence);
            m_Sequence = NULL;
        }
    }

    virtual bool GetNext(CTempString               & sequence,
                         CTempString               & ambiguities,
                         CRef<CBlast_def_line_set> & deflines,
                         vector<SBlastDbMaskData>  & mask_range,
                         vector<int>               & column_ids,
                         vector<CTempString>       & column_blobs);


    virtual void GetColumnNames(vector<string> & names) {}

    virtual int GetColumnId(const string & name){
        return m_Source->GetColumnId(name);
    }

    virtual const map<string,string> & GetColumnMetaData(int id) {
        return m_Source->GetColumnMetaData(id);
    }

    void ClearSequence() {
        if (m_Sequence) {
            _ASSERT(m_Source.NotEmpty());
            m_Source->RetSequence(& m_Sequence);
        }
    }

private:
    CRef<CSeqDBExpert> m_Source;
    const char * m_Sequence;
    int m_Oid;
};

CRawSeqDBSource::CRawSeqDBSource(const string & name,
                                 CBuildDatabase * outdb)
    : m_Sequence(NULL), m_Oid(0) {

    m_Source.Reset(new CSeqDBExpert(name, CSeqDB::eNucleotide));
}

bool
CRawSeqDBSource::GetNext(CTempString               & sequence,
                         CTempString               & ambiguities,
                         CRef<CBlast_def_line_set> & deflines,
                         vector<SBlastDbMaskData>  & mask_range,
                         vector<int>               & column_ids,
                         vector<CTempString>       & column_blobs)
{
    if (! m_Source->CheckOrFindOID(m_Oid))
        return false;

    if (m_Sequence) {
        m_Source->RetSequence(& m_Sequence);
        m_Sequence = NULL;
    }

    int slength(0), alength(0);
    m_Source->GetRawSeqAndAmbig(m_Oid, & m_Sequence, & slength, & alength);
    sequence    = CTempString(m_Sequence, slength);
    ambiguities = CTempString(m_Sequence + slength, alength);
    deflines = m_Source->GetHdr(m_Oid);
    m_Oid ++;

    return true;
}

void
CMakeBlastDBApp::x_VerifyInputFilesType(const string& seq_file) {
    // Guess the input data type
    CFile input_file(seq_file);
    if ( !input_file.Exists() ) {
        string error_msg = "File " + seq_file + " does not exist";
        NCBI_THROW(CInvalidDataException, eInvalidInput, error_msg);
    }
    if (input_file.GetLength() == 0) {
        string error_msg = "File " + seq_file + " is empty";
        NCBI_THROW(CInvalidDataException, eInvalidInput, error_msg);
    }
    CNcbiIfstream f(seq_file.c_str(), ios::binary);
    if(x_GuessFileType(f) != CFormatGuess::eFasta) {
        string error_msg = seq_file + " is not not in FASTA format.";
        NCBI_THROW(CInvalidDataException, eInvalidInput, error_msg);
    }

    return;
}

CMakeBlastDBApp::TFormat
CMakeBlastDBApp::x_GuessFileType(CNcbiIstream & input)
{
    CFormatGuess fg(input);
    fg.GetFormatHints().AddPreferredFormat(CFormatGuess::eFasta);
    fg.GetFormatHints().DisableAllNonpreferred();
    return fg.GuessFormat();
}

void CMakeBlastDBApp::x_ProcessInputData(const string & path) {
    x_VerifyInputFilesType(path);

    CNcbiIfstream file(path.c_str(), ios::binary);
    x_AddFasta(file);
}

void CMakeBlastDBApp::x_BuildDatabase(string in_file) {
    const CArgs & args = GetArgs();

    // N.B.: Source database(s) in the current working directory will
    // be overwritten (as in formatdb)
    // m_DB.Reset(new CBuildDatabase(in_file,
    //                               in_file,
    //                               false,
    //                               CWriteDB::eNoIndex,
    //                               false,
    //                               m_LogFile));

    m_DB.Reset(new CBuildDatabase(in_file,
                                  in_file,
                                  false,
                                  false,
                                  true,
                                  false,
                                  m_LogFile));

    TLinkoutMap no_bits;
    m_DB->SetLinkouts(no_bits, true);
    m_DB->SetMembBits(no_bits, true);

    // Max file size
    Uint8 bytes = NStr::StringToUInt8_DataSize(args["max_file_sz"].AsString());
    *m_LogFile << "Maximum file size: "
               << Uint8ToString_DataSize(bytes) << endl;

    m_DB->SetMaxFileSize(bytes);
    x_ProcessInputData(in_file);
}

string CMakeBlastDBApp::compute_unique_seqs(const string& in_file){
    *m_LogFile << std::endl << "Computing unique sequences in ";
    *m_LogFile << in_file << std::endl << std::endl;;
    CDirEntry infile(in_file);
    string unique_seqs_file = infile.GetDir() + "unique_" + infile.GetName();
    string asn_file = infile.GetDir() + "unique_" + infile.GetBase() + ".map";
    string short_seqs_file = infile.GetDir() + infile.GetBase() + ".short_seq";
    *m_LogFile << "Unique sequences:\t" << unique_seqs_file << std::endl;
    *m_LogFile << "Orig. sequences map:\t" << asn_file << std::endl;
    *m_LogFile << "Short sequences (removed):\t" << short_seqs_file << std::endl;
    ofstream out_short_seqs_stream(short_seqs_file.c_str(), ios::out);

    std::map< std::string, std::string > un_seqs;
    int num_seqs = 0;
    int num_short_seqs = 0;
    {{
            UniqueSeqs res;
            // STEP 2: open the file handler
            gzFile fp = gzopen(in_file.c_str(), "r");
            // STEP 3: initialize seq
            kseq_t* seq = kseq_init(fp);
            // STEP 4: read sequence
            while ((kseq_read(seq)) >= 0) {
                ++num_seqs;
		string s(seq->seq.s);
		if(s.length() < MIN_SEQ_LEN) {
		    num_short_seqs++;
		    out_short_seqs_stream << ">" << seq->name.s << "\n";
		    out_short_seqs_stream << seq->seq.s << "\n";
		} else {
		    if(un_seqs.find(seq->seq.s) == un_seqs.end()) {
			un_seqs[seq->seq.s] = seq->name.s;
			res.fasta_id_map[seq->name.s];
		    } else {
			res.fasta_id_map[un_seqs[seq->seq.s]].push_back(seq->name.s);
		    }
		}
            }
            kseq_destroy(seq); // STEP 5: destroy seq
            gzclose(fp); // STEP 6: close the file handler

            auto_ptr< CObjectOStream >
                outasn (CObjectOStream::Open(ASN_TYPE, asn_file));
            *outasn << res;
    }}

    std::map< std::string, std::string >::const_iterator it_seq;
    ofstream ofile(unique_seqs_file.c_str(), ios::out);
    for (it_seq = un_seqs.begin(); it_seq != un_seqs.end(); ++it_seq) {
        ofile << ">" << it_seq->second << std::endl;
        ofile << it_seq->first << std::endl;
    }
    *m_LogFile << "Num. orig. sequences:\t" << num_seqs << std::endl;
    *m_LogFile << "Num. short sequences (removed):\t" << num_short_seqs << std::endl;
    *m_LogFile << "Num. processed sequences:\t" << num_seqs - num_short_seqs << std::endl;
    *m_LogFile << "Num. unique sequences:\t" << un_seqs.size();
    *m_LogFile << " (~" << un_seqs.size()*100/(num_seqs  - num_short_seqs) << "%)" << std::endl;
    ofile.close();
    out_short_seqs_stream.flush();
    out_short_seqs_stream.close();
    return unique_seqs_file;
}

int CMakeBlastDBApp::Run(void) {
    int status = 0;
    const CArgs & args = GetArgs();
    if (args[kInput].AsString() == "-") {
        NCBI_THROW(CInvalidDataException, eInvalidInput,
                   "Please provide a correct input Fasta file.");
    }

    m_LogFile = & (args["logfile"].HasValue()
                   ? args["logfile"].AsOutputFile()
                   : cout);

    string in_file = args[kInput].AsString();
    try {
        if (args["uniqueseqs"]) {
            in_file = compute_unique_seqs(args[kInput].AsString());
        }
        x_BuildDatabase(in_file);
    }
    CATCH_ALL(status)
    return status;
}

int main(int argc, const char* argv[]) {
    return CMakeBlastDBApp().AppMain(argc, argv, 0, eDS_Default, 0);
}
