/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#include "score_comp.hpp"

#include <objmgr/scope.hpp>
#include <objects/seqalign/Seq_align.hpp>
#include <algo/align/nw/nw_formatter.hpp>
#include <util/sequtil/sequtil.hpp>
#include <objects/seq/Seq_data.hpp>

static ncbi::CRef<ncbi::objects::CSeq_entry>
    string_to_entry(const std::string& seq,
                    ncbi::CRef<ncbi::objects::CSeq_id> seq_id) {
    ncbi::CRef<ncbi::objects::CBioseq> m_CurrentSeq(new ncbi::objects::CBioseq);
    m_CurrentSeq->SetId().push_back(seq_id);
    ncbi::objects::CSeq_inst& inst = m_CurrentSeq->SetInst();

    inst.SetMol(ncbi::objects::CSeq_inst::eMol_na);
    inst.SetLength(seq.length());
    inst.SetRepr(ncbi::objects::CSeq_inst::eRepr_raw);
    ncbi::CRef<ncbi::objects::CSeq_data>
        sdata(new ncbi::objects::CSeq_data(seq,
                                           ncbi::objects::CSeq_data::e_Iupacna));

    inst.SetSeq_data(*sdata);

    ncbi::CRef<ncbi::objects::CSeq_entry> entry(new ncbi::objects::CSeq_entry);
    entry->SetSeq(*m_CurrentSeq);
    entry->Parentize();

    return entry;
}

ScoreComputation::TScore
    ScoreComputation::compute_score(const std::string& s1,
                                    const std::string& s2) {

    TScore retval;
    _aligner.SetSequences(s1, s2);
    retval.raw_score = _aligner.Run();
    ncbi::CRef<ncbi::objects::CSeq_entry> se1(string_to_entry(s1, seq_id1));
    ncbi::CRef<ncbi::objects::CSeq_entry> se2(string_to_entry(s2, seq_id2));

    ncbi::objects::CScope scope(*_objmgr);
    scope.AddTopLevelSeqEntry(*se1);
    scope.AddTopLevelSeqEntry(*se2);

    ncbi::CNWFormatter formatter(_aligner);
    formatter.SetSeqIds(seq_id1,
                        seq_id2);
    formatter.AsText(&retval.aln,
                     ncbi::CNWFormatter::eFormatType2,
                     _aligner.GetTranscriptString().length());
    //*** Enable the following lines to print the alignments
    // std::cout << retval.aln;

    ncbi::CRef<ncbi::objects::CSeq_align>
        sa(formatter.AsSeqAlign(0,
                                ncbi::objects::eNa_strand_plus,
                                0,
                                ncbi::objects::eNa_strand_plus));
    retval.bit_score = _sb.GetBlastBitScore(scope, *sa);
    retval.aln_len = _aligner.GetTranscriptString().length();
    return retval;
}
