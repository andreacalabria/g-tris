#!/bin/bash

###
 #
 #                           γ-TRIS
 #
 #       Graph-Algorithm Making Multi-labeling Annotations
 #             for Tracking Retroviral Integration Sites
 #
 # Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 #
 # Distributed under the terms of the GNU General Public License (or the Lesser
 # GPL).
 #
 # This file is part of γ-TRIS.
 #
 # γ-TRIS is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 # 
 # γ-TRIS is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 # GNU General Public License for more details.
 # 
 # You should have received a copy of the GNU General Public License
 # along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 #
###

# Organism
# options (case sensitive): human (default) and mouse
ORGANISM=human

#Execution Parameters
BLAST_PERC_ID=95 #Blast Identity Percentage
BLAST_EVALUE=0.01 #Blast Evalue
DEC_LEV=3 #Graph Decomposition Level
INIT_COV=0.75 #Consenus Inivital Coverage Threshold
INIT_FREQ=0.75 #Consenus Inivital Frequence Threshold
MED_COV=0.75 #Consenus Medium Ending Coverage Threshold
MED_FREQ=0.75 #Consenus Medium Ending Frequence Threshold
HIGH_COV=0.75 #Consenus High Ending Coverage Threshold
HIGH_FREQ=0.75 #Consenus High Ending Frequence Threshold
N_THREADS=1 #Number of threads
BLAST_BATCH_SIZE=2000 #BLAST batch size for seqalignment procedure

#Viral Filter Parameters (for iterations)
OPT_AR=("-perc_identity 99 -evalue 0.00001")

if [[ -f config.ini ]]
then
    source config.ini
fi

MAX_ITER=${#OPT_AR[*]}

abspath() (
    cd "$(dirname "$1")"
    printf "%s/%s\n" "${PWD}" "$(basename "$1")"
)


if [[ $# != 1 && $# != 3 ]]
then
    echo "Usage: exec_pipeline.sh <input_fasta_file> [ <input_r2_fasta_file> <input_tag_fasta_file> ]"
    exit 1
fi

for fin in "$@"
do
    if [[ ! -f ${fin} ]]
    then
	echo "File ${fin} not found."
	exit 1
    fi
done

if [[ ! -d ${PROG_DIR} ]]
then
    echo "CISDetection directory not found."
    exit 1
fi

if [[ ! -f ${VIRAL_GEN} ]]
then
    echo "Viral Genome not found."
    exit 1
fi

if [[ ! -f ${HUMAN_GEN} ]]
then
    echo "Human Reference Genome not found."
    exit 1
fi

#Correct FASTA headers
for fin in "$@"
do
    echo "Checking file ${fin}"
    if grep -q -e '[:()]' ${fin}
    then
	echo "Warning: ':', '(', and ')' changed into '_' in fasta headers."
	sed -e 's/[:()]/_/g' -i ${fin}
    fi

    if grep -q -e '[-]' ${fin}
    then
	echo "Warning: '-' changed into 'M' in fasta headers."
	sed -e 's/-/M/g' -i ${fin}
    fi

    if grep -q -e '[+]' ${fin}
    then
	echo "Warning: '+' changed into 'P' in fasta headers."
	sed -e 's/+/P/g' -i ${fin}
    fi
done

mkdir -pv iteration0
for fin in "$@"
do
    ln -sf $(abspath ${fin}) -t iteration0
done

F_DIR=./
F_EXT=`basename ${1} | awk -F . '{print $NF}'`
F_NAME=`basename ${1} .${F_EXT}`
#echo ${F_DIR}
#echo ${F_NAME}

for iter in $(seq 0 $((MAX_ITER-1)) )
do
    cd iteration${iter}

    #Execute makeblastdb
    PROG=makeblastdb
    echo "${PROG} -> unique sequences and initial index."
    CMD="${PROG_DIR}/${PROG} \
        -i ${F_DIR}/${F_NAME}.${F_EXT} \
        -uniqueseqs \
        &> ${F_DIR}/Exec_01_${PROG}.log"
    echo ${CMD}
    eval ${CMD}
    echo

    U_NAME=unique_${F_NAME}
    FILT_NAME=${U_NAME}.filt

    #Alignment to Viral Genome
    PROG=seqalignment
    echo "${PROG} -> alignment to viral genome."
    CMD="${PROG_DIR}/${PROG} \
        -i ${F_DIR}/${U_NAME}.${F_EXT} \
        -db ${VIRAL_GEN} \
        ${OPT_AR[$iter]} \
        -word_size 11 \
        -num_threads ${N_THREADS} \
        -batch_size ${BLAST_BATCH_SIZE} \
        &> ${F_DIR}/Exec_02_${PROG}_filter.log"
    echo ${CMD}
    eval ${CMD}
    echo

    #Remove Filtered Sequences
    PROG=seqfilter
    echo "${PROG} -> filter sequences based on viral alignments."
    CMD="${PROG_DIR}/${PROG} \
        -i ${F_DIR}/${U_NAME}.${F_EXT} \
        -aln ${F_DIR}/${U_NAME}.out.asn \
        &> ${F_DIR}/Exec_03_${PROG}.log"
    echo ${CMD}
    eval ${CMD}
    echo

    #Execute makeblastdb
    PROG=makeblastdb
    echo "${PROG} -> index of filtered sequences."
    CMD="${PROG_DIR}/${PROG} \
        -i ${F_DIR}/${FILT_NAME}.fa \
        &> ${F_DIR}/Exec_04_${PROG}_filter.log"
    echo ${CMD}
    eval ${CMD}
    echo

    #Execute seqalignment
    PROG=seqalignment
    echo "${PROG} -> all vs all alignment."
    CMD="${PROG_DIR}/${PROG} \
        -i ${F_DIR}/${FILT_NAME}.fa \
        -db ${F_DIR}/${FILT_NAME}.fa \
        -perc_identity ${BLAST_PERC_ID} \
        -evalue ${BLAST_EVALUE} \
        -word_size 11 \
        -num_threads ${N_THREADS} \
        -batch_size ${BLAST_BATCH_SIZE} \
        -forcestrandplus \
        &> ${F_DIR}/Exec_05_${PROG}.log"
    echo ${CMD}
    eval ${CMD}
    echo

    #Execute analyzegraph
    PROG=analyzegraph_SNAP
    echo "${PROG} -> graph decomposition."
    CMD="${PROG_DIR}/${PROG} \
        -i ${F_DIR}/${FILT_NAME}.out.asn \
        -m ${F_DIR}/${U_NAME}.map \
        -f ${F_DIR}/${FILT_NAME}.${F_EXT} \
        -dec_lev ${DEC_LEV} \
        &> ${F_DIR}/Exec_06_${PROG}.log"
    echo ${CMD}
    eval ${CMD}
    echo

    #Execute analyzeseqs
    PROG=analyzeseqs_SNAP
    echo "${PROG} -> consensus extraction."
    CMD="${PROG_DIR}/${PROG} \
        -subg ${F_DIR}/${FILT_NAME}.out.subg \
        -nodemap ${F_DIR}/${FILT_NAME}.out.nodemap \
        -num_threads ${N_THREADS} \
        -m ${F_DIR}/${U_NAME}.map \
        -init_cov ${INIT_COV} \
        -init_freq ${INIT_FREQ} \
        -med_cov ${MED_COV} \
        -med_freq ${MED_FREQ} \
        -high_cov ${HIGH_COV} \
        -high_freq ${HIGH_FREQ} \
        &> ${F_DIR}/Exec_07_${PROG}.log"
    echo ${CMD}
    eval ${CMD}
    echo

    #Consensus masking
    PROG=repeatmasker
    echo "${PROG} -> align consensus against RepBase and mask sequences with RepeatMasker."
    CMD="${PROG_DIR}/${PROG} \
        -engine hmmer \
        -parallel ${N_THREADS} \
        -species ${ORGANISM} \
        -dir ${F_DIR}/subgraphs/ \
        ${F_DIR}/subgraphs/consensus.fa \
        &> ${F_DIR}/Exec_08_${PROG}.log"
    echo ${CMD}
    eval ${CMD}
    echo

    #Execute iterativealignment
    PROG=iterativealignment
    echo "${PROG} -> iterative alignment of consensus sequences against human ref. genome."
    CMD="${PROG_DIR}/${PROG} \
        -i ${F_DIR}/subgraphs/consensus.fa \
        -db ${HUMAN_GEN} \
        -mask ${F_DIR}/subgraphs/consensus.fa.out \
        -num_threads ${N_THREADS} \
        -batch_size ${BLAST_BATCH_SIZE} \
        &> ${F_DIR}/Exec_09_${PROG}.log"
    echo ${CMD}
    eval ${CMD}
    echo

    #Execute createclusters
    PROG=createclusters_SNAP
    echo "${PROG} -> cluster creation."
    CMD="${PROG_DIR}/${PROG} \
        -i ${F_DIR}/subgraphs/consensus.out.asn \
        -subg  ${F_DIR}/${FILT_NAME}.out.subg \
        -f ${F_DIR}/subgraphs/consensus.fa \
        -m ${F_DIR}/${U_NAME}.map \
        -num_threads ${N_THREADS} \
        ${FINAL_CONSENSUS} \
        &> ${F_DIR}/Exec_10_${PROG}.log"
    echo ${CMD}
    eval ${CMD}
    echo

    #Execute analyzeclusterssimple
    PROG=analyzeclusterssimple
    echo "${PROG} -> cluster simple analysis."
    CMD="${PROG_DIR}/${PROG} \
        -i ${F_DIR}/${FILT_NAME}.out.all.clu \
        -label ${F_NAME} \
        -num_threads ${N_THREADS} \
        &> ${F_DIR}/Exec_11_${PROG}.log"
    echo ${CMD}
    eval ${CMD}
    echo

    if [[ $# == 3 ]]
    then
	PROG=computeTAG
	echo "${PROG} -> compute TAG count."
	CMD="${PROG_DIR}/${PROG} \
            -i ${F_DIR}/${FILT_NAME}.out.all.noprobl.clu \
            -t ${3} \
            -r ${2} \
            -m ${F_DIR}/${U_NAME}.map \
            -num_threads ${N_THREADS} \
            &> ${F_DIR}/Exec_12_${PROG}.log"
	echo ${CMD}
	eval ${CMD}
	echo
    fi

    #Execute analyzeclusters_paral
    PROG=analyzeclusters_paral
    echo "${PROG} -> cluster analysis."
    CMD="${PROG_DIR}/${PROG} \
        -i ${F_DIR}/${FILT_NAME}.out.clu \
        -m ${F_DIR}/${U_NAME}.map \
        -num_threads ${N_THREADS} \
        -init_cov ${INIT_COV} \
        -init_freq ${INIT_FREQ} \
        -med_cov ${MED_COV} \
        -med_freq ${MED_FREQ} \
        -high_cov ${HIGH_COV} \
        -high_freq ${HIGH_FREQ} \
        &> ${F_DIR}/Exec_11_${PROG}.log"
    #echo ${CMD}
    #eval ${CMD}
    echo

    if [[ -s ${F_DIR}/${FILT_NAME}.out.dist ]]
    then
        PROG=StatAnalysis.R
        CMD="Rscript ${PROG_DIR}/../${PROG} \
             ${F_DIR}/${FILT_NAME}.out.dist"
        #echo ${CMD}
        #eval ${CMD}
    fi

    cd ..
	
	#Execute analyzeclusters on merged clusters
	cd iteration${iter}
	if [[ -f ${FILT_NAME}.out.all.merged.clu ]]
	then
	    PROG=analyzeclusters_paral
	    #echo "${PROG} -> cluster analysis."
	    CMD="${PROG_DIR}/${PROG} \
                 -i ${F_DIR}/${FILT_NAME}.out.merged.clu \
                 -m ${F_DIR}/${U_NAME}.map \
                 -num_threads ${N_THREADS} \
                 -init_cov ${INIT_COV} \
                 -init_freq ${INIT_FREQ} \
                 -med_cov ${MED_COV} \
                 -med_freq ${MED_FREQ} \
                 -high_cov ${HIGH_COV} \
                 -high_freq ${HIGH_FREQ} \
                 &> ${F_DIR}/Exec_${PROG}_merged.log"
	    #echo ${CMD}
	    #eval ${CMD}
	    #echo
	fi

    if ((iter+1 < MAX_ITER))
    then
        mkdir -pv iteration$((iter+1))

        for cnm in `cat iteration${iter}/subgraphs/consensus.out.missing`
        do
            cat iteration${iter}/subgraphs/${cnm}.fa
        done > iteration$((iter+1))/${F_NAME}.${F_EXT}
    fi

done

#Export graph images
BLAST_GRAPH=iteration0/subgraphs/Subgraphs.dot
CLU_GRAPH=iteration0/${FILT_NAME}.out.dot

if [[ -s ${BLAST_GRAPH} && -s ${CLU_GRAPH} ]]
then
    echo "Graph picture creations."
    if hash neato 2>/dev/null
    then
        echo "Generating BLAST aln graph with neato."
        neato -Tpng ${BLAST_GRAPH} -o iteration0/Graph_blast.png
        if hash valgrind 2>/dev/null
        then
            echo "Generating Cluster creation graph with fdp."
            valgrind fdp -Tpdf ${CLU_GRAPH} -o iteration0/Graph_clusters.pdf
        else
            echo "Valgrind not found."
        fi
    else
        echo "Graphviz not found."
    fi
else
    echo "Dot file(s) not found or size equal to 0."
fi
