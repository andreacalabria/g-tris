######
# Makefile (GNU make is assumed!)
#
# Based on the idea/skeleton presented at
# http://mad-scientist.net/make/multi-arch.html
######

export OBJ_DIR := obj
ifneq ($(OBJ_DIR),$(notdir $(CURDIR)))

include target.mk

else
.DEFAULT: all

VPATH = $(SRC_DIR)

LOGFLAGS:= -DLOG_MSG -DLOG_THRESHOLD=LOG_LEVEL_DEBUG
INCLUDE_FLAGS:= -I$(LOC_DIR)/include/ \
		-I$(LOC_DIR)/include/ncbi-tools++/ \
		-I$(LOC_DIR)/include/clustalo/ \
                -I$(LOC_DIR)/include/snap-core/ \
                -I$(LOC_DIR)/include/glib-core/	\
		-I$(BASE_DIR)/include
# Pre-processor flags
CPPFLAGS= $(LOGFLAGS) $(INCLUDE_FLAGS)

# Common C and C++ flags
CCXXFLAGS:=-g -Wall -O2 -march=native -Wno-deprecated -ffunction-sections -fdata-sections -fopenmp
# C-only flags
CFLAGS+= $(CCXXFLAGS)
# C++-only flags
CXXFLAGS+= $(CCXXFLAGS)
# Linker flags
LDFLAGS+=-Wl,--gc-sections -fopenmp

# Define libraries
include $(BASE_DIR)/libs.mk

######
#
# LIST OF THE PROGRAMS
# For each listed program 'xxx', two variables must be defined:
# - OBJS_xxx = the object files that compose the program
# - LIBS_xxx = the libraries which must be linked
#
PROGRAMS:=makeblastdb	\
	seqalignment	\
	seqfilter	\
	iterativealignment	\
	analyzeclusters_paral \
	insertmissing \
	alignmentmissing \
	createsimpleclusters \
	mergeclusters \
	findclusters_SNAP \
	computematrix_SNAP \
	getmatrixlabel \
	analyzegraph_SNAP \
	analyzeseqs_SNAP \
	createclusters_SNAP \
	analyzeclusterssimple \
	computeTAG \
	repeatmasker

# seqalignment
OBJS_seqalignment= \
	log.o \
	configuration.o \
	alignments.o \
	iscluster.o \
	app_seqalignment.o
LIBS_seqalignment= $(LIBS)

# makeblastdb
OBJS_makeblastdb= \
	masked_range_set.o \
	blast_app_util.o \
	uniqueseqs.o \
	app_makeblastdb.o
LIBS_makeblastdb= $(LIBS)

# seqfilter
OBJS_seqfilter= \
	log.o \
	configuration.o \
	alignments.o \
	iscluster.o \
	app_seqfilter.o
LIBS_seqfilter= $(LIBS)

# iterativealignment
OBJS_iterativealignment= \
	log.o \
	configuration.o \
	alignments.o \
	iscluster.o \
	app_iterativealignment.o
LIBS_iterativealignment= $(LIBS) -lbwa

# analyzeclusters_paral
OBJS_analyzeclusters_paral= \
	log.o \
	configuration.o \
	alignments.o \
	iscluster.o \
	uniqueseqs.o \
	score_comp.o \
	app_analyzeclusters_paral.o
LIBS_analyzeclusters_paral= $(LIBS)

# insertmissing
OBJS_insertmissing= \
	log.o \
	configuration.o \
	alignments.o \
	iscluster.o \
	app_insertmissing.o
LIBS_insertmissing= $(LIBS)

# alignmentmissing
OBJS_alignmentmissing= \
	log.o \
	configuration.o \
	alignments.o \
	iscluster.o \
	app_alignmentmissing.o
LIBS_alignmentmissing= $(LIBS) -lbwa

# createsimpleclusters
OBJS_createsimpleclusters= \
	log.o \
	configuration.o \
	alignments.o \
	iscluster.o \
	uniqueseqs.o \
	app_createsimpleclusters.o
LIBS_createsimpleclusters= $(LIBS)

# mergeclusters
OBJS_mergeclusters= \
	log.o \
	configuration.o \
	alignments.o \
	iscluster.o \
	app_mergeclusters.o
LIBS_mergeclusters= $(LIBS)

# findclusters_SNAP
OBJS_findclusters_SNAP= \
	log.o \
	configuration.o \
	alignments.o \
	iscluster.o \
	app_findclusters_SNAP.o
LIBS_findclusters_SNAP= $(LIBS) $(LOC_DIR)/include/snap-core/Snap.o -lrt

# computematrix_SNAP
OBJS_computematrix_SNAP= \
        log.o \
        configuration.o \
        alignments.o \
        iscluster.o \
        app_computematrix_SNAP.o
LIBS_computematrix_SNAP= $(LIBS) $(LOC_DIR)/include/snap-core/Snap.o -lrt

# getmatrixlabel
OBJS_getmatrixlabel= \
        log.o \
        configuration.o \
        alignments.o \
        iscluster.o \
        app_getmatrixlabel.o
LIBS_getmatrixlabel= $(LIBS)

# analyzegraph SNAP library
OBJS_analyzegraph_SNAP = \
        log.o \
        configuration.o \
        alignments.o \
	uniqueseqs.o \
        app_analyzegraph_SNAP.o
LIBS_analyzegraph_SNAP= $(LIBS) $(LOC_DIR)/include/snap-core/Snap.o -lrt

# analyzeseqs SNAP library
OBJS_analyzeseqs_SNAP = \
        log.o \
        configuration.o \
        alignments.o \
	uniqueseqs.o \
        app_analyzeseqs_SNAP.o
LIBS_analyzeseqs_SNAP= $(LIBS) -lclustalo $(LOC_DIR)/include/snap-core/Snap.o -lrt

# createclusters SNAP library
OBJS_createclusters_SNAP= \
	log.o \
	configuration.o \
	alignments.o \
	iscluster.o \
	uniqueseqs.o \
	app_createclusters_SNAP.o
LIBS_createclusters_SNAP= $(LIBS) $(LOC_DIR)/include/snap-core/Snap.o -lrt

# analyzeclusterssimple
OBJS_analyzeclusterssimple= \
	log.o \
	configuration.o \
	alignments.o \
	iscluster.o \
	uniqueseqs.o \
	score_comp.o \
        app_analyzeclusterssimple.o
LIBS_analyzeclusterssimple= $(LIBS)

# computeTAG
OBJS_computeTAG= \
	log.o \
	configuration.o \
	alignments.o \
	iscluster.o \
	uniqueseqs.o \
	app_computeTAG.o
LIBS_computeTAG= $(LIBS) -lbwa
#
# END List of programs
######

.PHONY: all
all: $(addprefix $(BIN_DIR)/, $(PROGRAMS))

######
#
# Additional dependencies
$(BIN_DIR)/analyzeseqs_SNAP: $(BIN_DIR)/_clustalw2

$(BIN_DIR)/_clustalw2: $(LOC_DIR)/bin/clustalw2
	@cp $(LOC_DIR)/bin/clustalw2 $(BIN_DIR)/_clustalw2

$(BIN_DIR)/createclusters_SNAP: $(BIN_DIR)/_clustalw-mpi

$(BIN_DIR)/_clustalw-mpi: $(LOC_DIR)/bin/mpirun $(LOC_DIR)/bin/clustalw-mpi
	@ln -sf $(LOC_DIR)/bin/mpirun $(BIN_DIR)/mpirun
	@cp $(LOC_DIR)/bin/clustalw-mpi $(BIN_DIR)/_clustalw-mpi

$(BIN_DIR)/iterativealignment: $(BIN_DIR)/_bwa

$(BIN_DIR)/_bwa: $(LOC_DIR)/bin/bwa
	@cp $(LOC_DIR)/bin/bwa $(BIN_DIR)/_bwa

$(BIN_DIR)/repeatmasker: prerequisites
	@ln -sf $(3RD_DIR)/RepeatMasker/RepeatMasker $(BIN_DIR)/repeatmasker

$(BIN_DIR)/computeTAG: $(BIN_DIR)/_vsearch

$(BIN_DIR)/_vsearch: $(LOC_DIR)/bin/vsearch
	@cp $(LOC_DIR)/bin/vsearch $(BIN_DIR)/_vsearch

#
# END Additional dependencies
######


# Build the pre-requisites
.PHONY: prerequisites
prerequisites:
	@echo '* Building pre-requisites...' ; \
	$(MAKE) -C $(3RD_DIR) prerequisites

######
#
# Pattern rules.
# 1. build a .o file from a .cpp file with the same name (via CXX)
# 2. build a BIN_DIR/xxx file from OBJS_xxx and LIBS_xxx
#
######

.PRECIOUS: %.o
%.o: %.cpp
	@echo '* CXX $<'; \
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -o $@ -c $<

.SECONDEXPANSION:

$(BIN_DIR)/%: $$(OBJS_%)
	@echo '* LD  $@'; \
        [ -d $(BIN_DIR) ] || mkdir -p "$(BIN_DIR)" ; \
	$(CXX) $(LDFLAGS) -o $@ $(OBJS_$(notdir $@)) $(LIBS_$(notdir $@))


endif
