/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#include <corelib/ncbiobj.hpp>
#include <objmgr/object_manager.hpp>
#include <algo/align/nw/nw_aligner.hpp>
#include <algo/align/util/score_builder.hpp>
#include <objmgr/scope.hpp>
#include <objects/seqloc/Seq_id.hpp>

class ScoreComputation {
private:
// Const dummy IDs
    const ncbi::CRef<ncbi::objects::CSeq_id> seq_id1;
    const ncbi::CRef<ncbi::objects::CSeq_id> seq_id2;

    ncbi::objects::CScoreBuilder _sb;
    ncbi::CRef<ncbi::objects::CObjectManager> _objmgr;
    ncbi::CNWAligner _aligner;

public:

    typedef struct {
        int raw_score;
        double bit_score;
        std::string aln;
        int aln_len;
    } TScore;

    explicit ScoreComputation():
        seq_id1(new ncbi::objects::CSeq_id(ncbi::objects::CSeq_id::e_Local,
                                           "_id_1_")),
        seq_id2(new ncbi::objects::CSeq_id(ncbi::objects::CSeq_id::e_Local,
                                           "_id_2_")),
        _sb(ncbi::blast::eBlastn),
        _objmgr(ncbi::objects::CObjectManager::GetInstance())
    {
        _aligner.SetWm ( 2);
        _aligner.SetWms(-3);
        _aligner.SetWg (-5);
        _aligner.SetWs (-2);
        _aligner.SetScoreMatrix(NULL);
        _aligner.SetEndSpaceFree(false,  //L1
                                 false,  //R1
                                 false,  //L2
                                 false); //R2
    };

    TScore compute_score(const std::string& s1, const std::string& s2);

};
