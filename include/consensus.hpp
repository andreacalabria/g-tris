/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#ifndef _CONSENSUS_HPP_
#define _CONSENSUS_HPP_

#include "log.hpp"
#include "configuration.hpp"

#include <sstream>
#include <algorithm>

//Clustal Omega
extern "C" {
  #include "clustal-omega.h"
}

//Kseq fasta parser
#include <zlib.h>
#include <stdio.h>
#include "kseq.h"

KSEQ_INIT(gzFile, gzread)

static char *
readlink_malloc (const char *filename) {
  int size = 124;
  char *buffer = NULL;

  while (1) {
      buffer = (char *) realloc (buffer, size);
      int nchars = readlink(filename, buffer, size);
      if (nchars < 0) {
          free (buffer);
          return NULL;
      }
      if (nchars < size) {
          buffer[nchars]= '\0';
          return buffer;
      }
      size *= 2;
      buffer[0] = '\0';
  }
}

struct MultipleSequenceAligner_ClustalOmega {
private:
    const unsigned int threads;
public:
    MultipleSequenceAligner_ClustalOmega(const unsigned int _threads= 1u)
        :threads(_threads)
    {}

    void operator()(const std::map< string,
                                    std::list< string > >& fasta_id_map,
                    const string& fasta_file,
                    std::vector< string >& aln_id,
                    std::vector< string >& aln_seq,
                    const bool print) const {
        //Multiple sequence structure
        mseq_t *prMSeq = NULL;
        //Alignment options to use
        opts_t rAlnOpts;
        //Setup logger
        LogDefaultSetup(&rLog);
        memset(&rAlnOpts, 0, sizeof(opts_t));
        SetDefaultAlnOpts(&rAlnOpts);
        InitClustalOmega(threads);
        //PrintAlnOpts(stdout, &rAlnOpts);

        //Read sequence file
        NewMSeq(&prMSeq);
        prMSeq->aligned = false;
        char *cstr = new char[fasta_file.length() + 1];
        strcpy(cstr, fasta_file.c_str());
        ReadSequences(prMSeq, cstr, SEQTYPE_DNA, SQFILE_FASTA,
                      false, INT_MAX, INT_MAX);

        //Align the sequences without a profile (NULL)
        if (Align(prMSeq, NULL, &rAlnOpts)) {
            ERROR("Clustal Omega alignment error.");
        }
        if(!prMSeq->aligned) {
            ERROR("Sequcences not aligned.");
        }
        //Write Clustal Omega alignment
        for (int iAux=0; iAux<prMSeq->nseqs; iAux++) {
            if (print) {
                std::cout << prMSeq->sqinfo[iAux].name << "\t  ";
                std::cout << prMSeq->seq[iAux] << "\t";
                std::cout << prMSeq->sqinfo[iAux].len << "\t";
                int w = fasta_id_map.at(prMSeq->sqinfo[iAux].name).size() + 1;
                std::cout << w << "\t";
                std::cout << "\n";
            }
            aln_id.push_back(prMSeq->sqinfo[iAux].name);
            aln_seq.push_back(prMSeq->seq[iAux]);
        }
	std::cout << std::endl;
        FreeMSeq(&prMSeq);
    }
};


struct MultipleSequenceAligner_ClustalW {
private:
    const std::string path;
public:
    MultipleSequenceAligner_ClustalW(const std::string& _path)
        :path(_path)
    {}

    void operator()(const std::map< string,
                                    std::list< string > >& fasta_id_map,
                    const string& fasta_file,
                    std::vector< string >& aln_id,
                    std::vector< string >& aln_seq,
                    const bool print) const {
        //Path of 'clustalw2' program
        string clustalWprog = path + "_clustalw2";
        const std::string basic_options = "-TYPE=DNA -OUTPUT=FASTA";
        //Input file parameter
        string inFile = "-INFILE=" + fasta_file;
        //Output file
        CFile in_file(fasta_file);
        assert(in_file.Exists());
        if (print) {
            std::cout << "File size: " << in_file.GetLength() << std::endl;
        }
        string out_file(in_file.GetDir() +
                        in_file.GetBase() +
                        ".aln");
        //Output file parameter
        string outFile("-OUTFILE=" +
                       out_file);
        //Execution command
        string clustalw(clustalWprog + " " +
                        basic_options +  " " +
                        inFile + " " + outFile + " > /dev/null");
        if (print) {
            std::cout << "Clustalw call:\n" << clustalw << std::endl;
        }

        if (system(clustalw.c_str())) {
            ERROR("Clustalw invocation error.");
        }

        try{
                gzFile fp = gzopen(out_file.c_str(), "r");
                kseq_t* seq = kseq_init(fp);
                while ((kseq_read(seq)) >= 0) {
                    string id = seq->name.s;
                    string s = seq->seq.s;
                    int weight = fasta_id_map.at(id).size() + 1;
                    aln_id.push_back(id);
                    aln_seq.push_back(s);
                    if (print) {
                        std::cout << id << "\t  ";
                        std::cout << s << "\t";
                        std::cout << s.length() << "\t";
                        std::cout << weight << "\t";
                        std::cout << "\n";
                    }
                }
                gzclose(fp);
        } catch(exception& e) {
          ERROR("Clustalw fasta file error: " << e.what());
        }
    }
};

struct MultipleSequenceAligner_ClustalW_MPI {
private:
    const std::string path;
    const int threads;
public:
    MultipleSequenceAligner_ClustalW_MPI(const std::string& _path,
					 const unsigned int _threads)
        :path(_path),threads(_threads)
    {}

    void operator()(const std::map< string,
                                    std::list< string > >& fasta_id_map,
                    const string& fasta_file,
                    std::vector< string >& aln_id,
                    std::vector< string >& aln_seq,
                    const bool print) const {
	int num_seqs = 0;
	try{
	    gzFile fp = gzopen(fasta_file.c_str(), "r");
	    kseq_t* seq = kseq_init(fp);
	    while ((kseq_read(seq)) >= 0) {
		num_seqs++;
	    }
	    kseq_destroy(seq);
	    gzclose(fp);
        } catch(exception& e) {
	    ERROR("Clustalw fasta file error: " << e.what());
        }
	int real_threads = min(num_seqs, threads);

        //Path of 'clustalw-mpi' program
        stringstream clustalWprog;
	clustalWprog << path << "mpirun -np " << real_threads
		     << " " <<	path << "_clustalw-mpi";
        const std::string basic_options = "-TYPE=DNA -OUTPUT=FASTA";
        //Output file
        CFile in_file(fasta_file);
        assert(in_file.Exists());
        if (print) {
            std::cout << "File size: " << in_file.GetLength() << std::endl;
        }
        string out_tmp_file(in_file.GetDir() +
			    in_file.GetBase() +
			    "_tmp.aln");

	string out_file(in_file.GetDir() +
			in_file.GetBase() +
			".aln");

        //Execution command
        string clustalw_mpi(clustalWprog.str() + " " +
			    basic_options +  " " +
			    "-INFILE=" + fasta_file + " " +
			    "-OUTFILE=" + out_tmp_file + " > /dev/null");
	
	string clustalwConvProg = path + "_clustalw2 -CONVERT ";
	string clustalwConv(clustalwConvProg + " " +
			    basic_options + " " +
			    "-INFILE=" + out_tmp_file + " " +
			    "-OUTFILE=" + out_file +  " > /dev/null");
	
        if (print) {
            std::cout << "Clustalw-mpi call:\n" << clustalw_mpi << std::endl;
	    std::cout << "Clustalw convert call:\n" << clustalwConv << std::endl;
        }

	string final_call = clustalw_mpi + "; " + clustalwConv;
	
        if (system(final_call.c_str())) {
            ERROR("Clustalw invocation error.");
        }

        try{
                gzFile fp = gzopen(out_file.c_str(), "r");
                kseq_t* seq = kseq_init(fp);
                while ((kseq_read(seq)) >= 0) {
                    string id = seq->name.s;
                    string s = seq->seq.s;
                    int weight = fasta_id_map.at(id).size() + 1;
                    aln_id.push_back(id);
                    aln_seq.push_back(s);
                    if (print) {
                        std::cout << id << "\t  ";
                        std::cout << s << "\t";
                        std::cout << s.length() << "\t";
                        std::cout << weight << "\t";
                        std::cout << "\n";
                    }
                }
		kseq_destroy(seq);
                gzclose(fp);
        } catch(exception& e) {
          ERROR("Clustalw fasta file error: " << e.what());
        }
    }
};


template <typename MultipleSequenceAligner>
string representativeString(const std::map< string,
                                            std::list< string > >& fasta_id_map,
                            const string& subg_file,
                            const MultipleSequenceAligner& msa,
                            const double init_cov,
                            const double init_freq,
                            const double med_cov,
                            const double med_freq,
                            const double high_cov,
                            const double high_freq,
                            const bool print) {
    string consensus = "";

    //Multiple sequence structure
    std::vector< string > aln_id;
    std::vector< string > aln_seq;

    //msaClustalOmega(fasta_id_map, subg_file, 1, aln_id, aln_seq);
    //msaClustalW(fasta_id_map, subg_file, aln_id, aln_seq);

    msa(fasta_id_map, subg_file, aln_id, aln_seq, print);

    int seqs_starts[aln_seq.size()];
    int seqs_ends[aln_seq.size()];
    int aln_len = aln_seq[0].length();
    for (unsigned int iAux = 0; iAux < aln_seq.size(); ++iAux) {
        seqs_starts[iAux] = 0;
        seqs_ends[iAux] = 0;
    }
    for (unsigned int iAux = 0; iAux < aln_seq.size(); ++iAux) {
        for(unsigned int i = 0; i < aln_seq[iAux].length(); ++i) {
            char ch = aln_seq[iAux][i];
            if (((ch >= 'a') && (ch <= 'z')) || ((ch >= 'A') && (ch <= 'Z'))) {
                seqs_starts[iAux] = i;
                break;
            }
        }
        for (unsigned int i = aln_seq[iAux].length() - 1; i >= 0; --i) {
            char ch = aln_seq[iAux][i];
            if (((ch >= 'a') && (ch <= 'z')) || ((ch >= 'A') && (ch <= 'Z'))) {
                seqs_ends[iAux] = i;
                break;
            }
        }
    }
    int cons_freq[aln_len];
    char cons_chr[aln_len];
    int seqs_cover[aln_len];
    int sum_cover = 0;
    for (int i = 0; i < aln_len; ++i) {
        seqs_cover[i] = 0;
        cons_freq[i] = 0;
    }
    for (unsigned int iAux = 0; iAux < aln_seq.size(); ++iAux) {
        int seq_w = fasta_id_map.at(aln_id[iAux]).size() + 1;
        sum_cover += seq_w;
        for (int i = seqs_starts[iAux]; i <= seqs_ends[iAux]; ++i) {
            seqs_cover[i] += seq_w;
        }
    }
    // for (int i = 0; i < aln_len; ++i) {
    //     std::cout << seqs_cover[i];;
    // }
    // std::cout << endl;

    for (int i = 0; i < aln_len; ++i) {
        int cons_matr[18] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        for (unsigned int iAux = 0; iAux < aln_seq.size(); iAux++) {
            if(i >= seqs_starts[iAux] && i <= seqs_ends[iAux]) {
                int weight = fasta_id_map.at(aln_id[iAux]).size() + 1;
                //Convert to upper case: 'toupper'
                switch(aln_seq[iAux][i]) {
                case 'A':
                    cons_matr[0] += weight;
                    break;
                case 'C':
                    cons_matr[1] += weight;
                    break;
                case 'G':
                    cons_matr[2] += weight;
                    break;
                case 'T':
                    cons_matr[3] += weight;
                    break;
                case 'U':
                    cons_matr[4] += weight;
                    break;
                case 'R':
                    cons_matr[5] += weight;
                    break;
                case 'Y':
                    cons_matr[6] += weight;
                    break;
                case 'K':
                    cons_matr[7] += weight;
                    break;
                case 'M':
                    cons_matr[8] += weight;
                    break;
                case 'S':
                    cons_matr[9] += weight;
                    break;
                case 'W':
                    cons_matr[10] += weight;
                    break;
                case 'B':
                    cons_matr[11] += weight;
                    break;
                case 'D':
                    cons_matr[12] += weight;
                    break;
                case 'H':
                    cons_matr[13] += weight;
                    break;
                case 'V':
                    cons_matr[14] += weight;
                    break;
                case 'N':
                    cons_matr[15] += weight;
                    break;
                case 'X':
                    cons_matr[16] += weight;
                    break;
                case '-':
                    cons_matr[17] += weight;
                    break;
                default:
                    ERROR("Unknown character: '" << aln_seq[iAux][i] << "'");
                    break;
                }
            }
        }
        int max_pos = 0;
        for(int j = 1; j < 18; ++j) {
            if(cons_matr[j] > cons_matr[max_pos]) {
                max_pos = j;
            }
        }
        cons_freq[i] = cons_matr[max_pos];
        cons_chr[i] = fasta_nucl[max_pos];
    }
    //Compute consensus start position
    int cons_pos = 0;
    bool start = false;
    while(!start && cons_pos < aln_len) {
        if(seqs_cover[cons_pos] >= init_cov * sum_cover &&
           cons_freq[cons_pos] >= init_freq * seqs_cover[cons_pos]) {
            start = true;
        }
        ++cons_pos;
    }
    //if(cons_pos > PCR_THR) {
    //    WARN("Consensus starting position is grater than " << PCR_THR);
    //}
    --cons_pos;

    //Compute consensus from start position
    int num_ins_n = 0;
    int last_not_n = 0;
    while(num_ins_n <= MAX_INS_N && cons_pos < aln_len){
        if(((seqs_cover[cons_pos] >= high_cov * sum_cover &&
             cons_freq[cons_pos] >= med_freq * seqs_cover[cons_pos])
            ||
            (seqs_cover[cons_pos] >= med_cov * sum_cover &&
             cons_freq[cons_pos] >= high_freq * seqs_cover[cons_pos]))
           &&
           (seqs_cover[cons_pos] >= END_ABS_W_SEQ_THR &&
            cons_freq[cons_pos] >= END_ABS_W_SYM_THR)
           ) {
            if(cons_chr[cons_pos] != '-') {
                consensus += cons_chr[cons_pos];
            }
            last_not_n = consensus.length();
        } else {
            consensus += "N";
            ++num_ins_n;
        }
        ++cons_pos;
    }
    consensus = consensus.substr(0, last_not_n);

    return consensus;
}

struct RepresentativeSeqs {
    int weight;
    string id;
    string seq;
};

class CompareRepresentativeSeqs {
public:
    bool operator()(RepresentativeSeqs& r1,
		    RepresentativeSeqs& r2) {
	if(r2.weight < r1.weight) {
	    return true;
	}
	return false;
    }
};

#endif
