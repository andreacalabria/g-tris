/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#ifndef _ISCLUSTER_HPP_
#define _ISCLUSTER_HPP_

#include "log.hpp"
#include "configuration.hpp"
#include "alignments.hpp"
#include "uniqueseqs.hpp"
#include <set>
#include <cmath>

//Label representing a consensus alignment
class Label {
public:
    string target_id; //Target sequence (chr#)
    double centroid; //Alignment position
    int offset; // Positive or negative
    int aln_score; //Score
    std::list< string > seq_id; //List of cluster id having this label

    //Constructors
    explicit Label(const string& clu_id = "",
                   const AlnData& al_data = AlnData()) :
        target_id(al_data.target_id),
        aln_score(al_data.score),
        seq_id() {
        if (al_data.target_strand == al_data.query_strand) {
            centroid = al_data.target_start;
            offset = al_data.alignment_len;
        } else {
            centroid = al_data.target_stop;
            offset = -al_data.alignment_len;
        }
        seq_id.push_back(clu_id);
    }

    Label(const string& _target_id,
          const double _centroid,
          const int _offset,
          const int _aln_score,
          const std::list< string >& _seq_id) :
        target_id(_target_id),
        centroid(_centroid),
        offset(_offset),
        aln_score(_aln_score),
        seq_id(_seq_id) {}

    //Overloading operator <
    bool operator<(const Label& l1) const {
        return centroid < l1.centroid;
    }

    DECLARE_INTERNAL_TYPE_INFO();
};


//Class representing a cluster
class ISCluster {
public:
    string clu_id; //Cluster id
    string cons_seq; // Consensus sequence
    int num_aln; // Number of alignments (initial)
    int max_aln_score; // NOT USED - Max score of alignments
    int seq_len; // Length of the consensus
    int weight; // Weight of the subgraph (num. of sequences)
    double clu_score; // Cluster score (computed on previous paramters)
    std::vector< Label > lab; // Vector of labels (alignments)
    std::list< string > merged; // List of merged clusters
    //Masked
    std::list< string > masked; // List of RepeatMasker labels

    //Constructors
    explicit ISCluster(const string& _clu_id = "",
		       const int _weight = 0,
		       const Alignments& al = Alignments()) :
        clu_id(_clu_id),
        cons_seq(""),
        num_aln(al.al_data.size()),
        max_aln_score(0),
	seq_len(0),
        weight(_weight),
        clu_score(0),
        lab(),
        merged(),
        masked(al.masked) {
        for (std::list< AlnData >::const_iterator it = al.al_data.begin();
            it != al.al_data.end(); ++it) {
            lab.push_back(Label(_clu_id, *it));
            if (it->score > max_aln_score) {
              max_aln_score = it->score;
            }
        }
        merged.push_back(_clu_id);
    }

    ISCluster(const string& _clu_id,
	      const int _num_aln,
	      const int _max_aln_score,
	      const int _weight,
	      const Label& l,
	      const std::list< string >& _masked) :
        clu_id(_clu_id),
        cons_seq(""),
        num_aln(_num_aln),
        max_aln_score(_max_aln_score),
	seq_len(0),
        weight(_weight),
        clu_score(0),
        lab(),
        merged(),
        masked(_masked) {
        lab.push_back(l);
        merged.push_back(_clu_id);
    }

    DECLARE_INTERNAL_TYPE_INFO();
};

ostream& operator<<(ostream& os, const ISCluster& cluster);

struct SingleClusterOperations {
private:
    static ofstream null_stream;
    ofstream& out_dot_stream;
    void processCommonLabels(std::vector< std::multimap< unsigned int, unsigned int > >& lab1_adj,
			     unsigned int lab1_entry,
			     std::vector< std::multimap< unsigned int, unsigned int > >& lab2_adj,
			     unsigned int lab2_entry);
    void createNewLabel(std::set< pair< string, string > >& dot_edges,
			std::vector< Label >& new_labs,
			ISCluster& c1,
			int lab1,
			ISCluster& c2,
			int lab2);
	
public:
    SingleClusterOperations(ofstream& _out_dot_stream = null_stream) :
	out_dot_stream(_out_dot_stream) {}
    
    bool checkSingleClusters_old(const ISCluster& c1, const ISCluster& c2);
	
    bool mergeSingleClusters_old(ISCluster& c1, ISCluster& c2);

    bool checkSingleClusters(const ISCluster& c1, const ISCluster& c2);

    bool mergeSingleClusters(ISCluster& c1, ISCluster& c2);
};

int refineClusters(std::list< ISCluster >& clu,
		   ofstream& out_dot_stream);

void computeSimpleClusterScore(std::list< ISCluster >& clu);

int computeClusterConsensus(std::list< ISCluster >& clu,
			    std::map< string, std::list< string > >& fasta_id_map,
			    const std::map< string, string >& cons_seq,
			    const string& subg_dir_name,
			    int num_threads);

#endif
