/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#ifndef _ALIGNMENTS_HPP_
#define _ALIGNMENTS_HPP_

#include "configuration.hpp"
#include <algo/blast/format/blast_format.hpp>
#include <serial/serialimpl.hpp>

//Alignment data class
class AlnData {
    public:
    //ID of the target sequence (to which the query sequence maps)
    string target_id;
    //Query alignment starting position
    int query_start;
    //Query alignment ending position
    int query_stop;
    //Target alignment starting position
    int target_start;
    //Target alignment ending position
    int target_stop;
    //Query strand (-1 / +1)
    int query_strand;
    //Target strand (-1 / +1)
    int target_strand;
    //Alignment length
    int alignment_len;
    //Alignment score
    int score;

    //Constructor
    explicit AlnData(const string& target_id_ = "",
                     const int query_start_ = -1,
                     const int query_stop_ = -1,
                     const int target_start_ = -1,
                     const int target_stop_ = -1,
                     const int query_strand_ = 0,
                     const int target_strand_ = 0,
                     const int alignment_len_ = -1,
                     const int score_ = 0) :
        target_id(target_id_),
        query_start(query_start_),
        query_stop(query_stop_),
        target_start(target_start_),
        target_stop(target_stop_),
        query_strand(query_strand_),
        target_strand(target_strand_),
        alignment_len(alignment_len_),
        score(score_) {};

    DECLARE_INTERNAL_TYPE_INFO();
};

//Alignment class
class Alignments {
public:
    //ID of the query sequence
    string query_id;
    //Length of the query sequence
    int query_len;
    //Masked
    std::list< string > masked;
    //Max score (keep only amx-score aln)
    int max_score;
    //Vector of alignment data (instances)
    std::list< AlnData > al_data;
    DECLARE_INTERNAL_TYPE_INFO();

};

#ifdef PRINT_CSV_ALN
//Print alignments in CSV format
void printAln(const Alignments& a, ostream& os);
#endif

#endif
