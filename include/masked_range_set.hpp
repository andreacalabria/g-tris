/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#ifndef _MASKED_RANGE_SET_HPP_
#define _MASKED_RANGE_SET_HPP_

#include <objtools/blast/seqdb_writer/build_db.hpp>

USING_NCBI_SCOPE;
USING_SCOPE(objects);

/** @file masked_range_set.hpp
  FIXME
*/

// First version will be simple -- it will use universal Seq-loc
// logic.  If performance is a problem then it might be best to have
// two containers, one with a Seq-loc key and one with an integer key.

class CMaskedRangeSet : public IMaskDataSource {
public:
    void Insert(int              algo_id,
                const CSeq_id  & id,
                const CSeq_loc & v)
    {
        x_CombineLocs(x_Set(algo_id, CSeq_id_Handle::GetHandle(id)), v);
    }
    
    virtual CMaskedRangesVector &
    GetRanges(const list< CRef<CSeq_id> > & idlist);
    
private:
    void x_FindAndCombine(CConstRef<CSeq_loc> & L1, int algo_id,
                          CSeq_id_Handle& id);
    
    static void x_CombineLocs(CConstRef<CSeq_loc> & L1, const CSeq_loc & L2);
    
    CConstRef<CSeq_loc> & x_Set(int algo_id, CSeq_id_Handle id);
    
    typedef map< CSeq_id_Handle, CConstRef<CSeq_loc> > TAlgoMap;
    
    CMaskedRangesVector m_Ranges;
    vector< TAlgoMap > m_Values;
};

#endif /* _MASKED_RANGE_SET_HPP_ */
