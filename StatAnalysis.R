###
 #
 #                           γ-TRIS
 #
 #       Graph-Algorithm Making Multi-labeling Annotations
 #             for Tracking Retroviral Integration Sites
 #
 # Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 #
 # Distributed under the terms of the GNU General Public License (or the Lesser
 # GPL).
 #
 # This file is part of γ-TRIS.
 #
 # γ-TRIS is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 # 
 # γ-TRIS is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 # GNU General Public License for more details.
 # 
 # You should have received a copy of the GNU General Public License
 # along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 #
###

library("ggplot2")
args <- commandArgs(trailingOnly = TRUE)
filename <- args[1]
out.dir <- paste(getwd(), "/Statistics/", sep="")
dir.create(out.dir)
color_names <- c("O"="red", "S"="blue")
t <- read.csv(filename, sep = "\t", header = F)
colnames(t) <- c("cpair", "bitscore", "score", "type")
t$cpair <- as.character(t$cpair)
t$type <- as.character(t$type)

create.my.plot <- function(t, file.name) {
    orig.bitscore <- t$bitscore[t$type == "O"][1];
    ts <- t[t$bitscore != orig.bitscore | t$type == "O",]
    binwidth <- min(1, (max(ts$bitscore) - min(ts$bitscore))/ 40)
    p <- (  ggplot(data = ts, aes(x = cpair, y = bitscore, colour = type))
          + geom_violin(colour = "lightblue")
#          + geom_point(position = position_jitter(w = 0.1, h = 0))
          + geom_dotplot(binaxis="y", stackdir = "center", binwidth=binwidth,
                         stackratio=0.4,
                         aes(fill=type),method="histodot")
          + scale_colour_manual(values = color_names,
                                labels = c("Original", "Stat. Analysis"),
                                name = "Type")
          + scale_fill_manual(values = color_names,
                                labels = c("Original", "Stat. Analysis"),
                                name = "Type")
          + geom_hline(yintercept=orig.bitscore, colour="red")
#          + geom_point(y=orig.bitscore, aes(colour="O"))
          + annotate("text",
                     x = t$cpair,
                     y = orig.bitscore + 8*binwidth,
                     label = sum(ts$bitscore > orig.bitscore))
          + annotate("text",
                     x = t$cpair,
                     y = orig.bitscore - 8*binwidth,
                     label = sum(ts$bitscore < orig.bitscore))
          + xlab(NULL)
          + ylab("BitScore")
          + theme(legend.position="top",
                  legend.background = element_rect(fill="gray90", size=.5, linetype="dotted"))
          );
    ggsave(filename = paste(out.dir, "plot-", file.name, ".png", sep=""), plot = p,
           width = 13, height = 13, units = "cm", dpi = 600);
    TRUE;
}

pair.names <- unique(t$cpair);
for(current.pair in pair.names) {
    current.t <- subset(t, cpair == current.pair);
    print(current.pair);
    if (NROW(current.t)<=2 || NROW(unique(current.t$bitscore))<2 ) {
        print(paste("Problems with", current.pair))
    } else {
        create.my.plot(current.t, current.pair)
    }
}
